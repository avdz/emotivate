package com.goodwill.tristan.emotivate

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.facebook.stetho.Stetho
import com.google.android.gms.ads.MobileAds
import com.microsoft.appcenter.AppCenter
import com.microsoft.appcenter.analytics.Analytics
import com.microsoft.appcenter.crashes.Crashes
import com.goodwill.tristan.emotivate.data.api.RestApiClient
import com.goodwill.tristan.emotivate.data.shared_preferences.base.EmotivateSharedPreferences
import com.goodwill.tristan.emotivate.utilities.managers.EmotivateNotificationManager
import com.uphyca.stetho_realm.RealmInspectorModulesProvider
import io.fabric.sdk.android.Fabric
import io.realm.Realm
import io.realm.RealmConfiguration
import java.util.regex.Pattern



/**
 * Created by Comp on 24.7.2018..
 */
class EmotivateApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        RestApiClient.init()
        MobileAds.initialize(this)
        EmotivateSharedPreferences.init(this)
        Fabric.with(this, Crashlytics())
        val realmInspector = RealmInspectorModulesProvider.builder(this)
                .withFolder(cacheDir)
                .withMetaTables()
                .withDescendingOrder()
                .withLimit(1000)
                .databaseNamePattern(Pattern.compile(".+\\.realm"))
                .build()

        val realmConfig = RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build()

        Realm.setDefaultConfiguration(realmConfig)

        Stetho.initialize(Stetho.newInitializerBuilder(this)
                .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
                .enableWebKitInspector(realmInspector)
                .build())

        EmotivateNotificationManager(this).setupNotificationChannels()

        AppCenter.start(this, "601ccb0c-1321-4d8d-8b6b-aa8066c803c0",
                Analytics::class.java, Crashes::class.java)
        AppCenter.start(this, "601ccb0c-1321-4d8d-8b6b-aa8066c803c0", Analytics::class.java, Crashes::class.java)
    }
}