package com.goodwill.tristan.emotivate.data.api

import com.goodwill.tristan.emotivate.data.api.requests.CreateQuoteRequest
import com.goodwill.tristan.emotivate.data.api.requests.LoginRequest
import com.goodwill.tristan.emotivate.data.api.requests.RegisterRequest
import com.goodwill.tristan.emotivate.data.api.requests.ResetPasswordRequest
import com.goodwill.tristan.emotivate.data.api.response.AuthorsResponse
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.User
import com.goodwill.tristan.emotivate.models.firebasemodels.NotificationRequest
import okhttp3.Response
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*


/**
 * Created by Azra on 1.8.2019.
 */
interface EndPoints {

    @GET("authors")
    fun getAuthors(@Query("page") page : Int?,
                   @Query("per_page") perPage : Int?,
                   @Query("q") query : String?) : Call<AuthorsResponse>

    @GET("categories")
    fun getCategories() : Call<ArrayList<Category>>

    @GET("quotes")
    fun getQuotes(@Query("page") page : Int?,
                  @Query("per_page") perPage : Int?,
                  @Query("q") query : String?) : Call<QuotesResponse>

    @GET("quotes")
    fun getQuotes(@Query("page") page : Int?,
                  @Query("per_page") perPage : Int?,
                  @Query("q") query : String?,
                  @Query("category_id") categoryId : Int?) : Call<QuotesResponse>

    @POST("quotes")
    fun createQuote(@Body createQuoteRequest: CreateQuoteRequest) : Call<ResponseBody>

    @DELETE("quotes/{id}")
    fun deleteQuote(@Path("id") id : String) : Call<ResponseBody>

    @GET("quotes")
    fun getAuthorQuotes(@Query("author_id") authorId : String,
                        @Query("page") page : Int?,
                        @Query("per_page") perPage : Int?,
                        @Query("q") query : String?) : Call<QuotesResponse>


    @GET("quotes/favorites")
    fun getMyQuotes() : Call<QuotesResponse>

    @POST("login")
    fun login(@Body loginRequest: LoginRequest) : Call<User>

    @POST("register")
    fun register(@Body registerRequest: RegisterRequest) : Call<ResponseBody>

    @POST("reset_password")
    fun resetPassword(@Body resetPasswordRequest: ResetPasswordRequest) : Call<ResponseBody>

    @POST("notify")
    fun getNotification(@Body notificationRequest : NotificationRequest) : Call<ResponseBody>


}