package com.goodwill.tristan.emotivate.data.api

import com.goodwill.tristan.emotivate.data.shared_preferences.UserStorage
import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Azra on 1.8.2019.
 */
class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val user = UserStorage.getUser()
        return if (user != null) {
            val request = chain?.request()?.newBuilder()
                    ?.addHeader("Accept", "*/*")
                    ?.addHeader("Content-Type", "application/json")
                    ?.addHeader("Authorization", "Bearer " + user.accessToken)
                    ?.build()
            chain?.proceed(request!!)!!
        } else {
            val request = chain?.request()?.newBuilder()
                    ?.addHeader("Accept", "*/*")
                    ?.addHeader("Content-Type", "application/json")
                    ?.build()
            chain?.proceed(request!!)!!
        }
    }

}