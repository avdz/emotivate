package com.goodwill.tristan.emotivate.data.api

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.*
import com.goodwill.tristan.emotivate.data.api.response.AuthorsResponse
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.User
import com.goodwill.tristan.emotivate.models.firebasemodels.NotificationRequest
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback

/**
 * Created by Azra on 1.8.2019.
 */
class HttpService {
    companion object {
        fun getAuthors(authorsRequest : GetAuthorsRequest) {
            val call : Call<AuthorsResponse> = RestApiClient.getService().getAuthors(
                    authorsRequest.page,
                    authorsRequest.perPage,
                    authorsRequest.query)
            call.enqueue(authorsRequest.callback)
        }

        fun getCategories(callback : Callback<ArrayList<Category>>) {
            val call : Call<ArrayList<Category>> = RestApiClient.getService().getCategories()
            call.enqueue(callback)
        }

        fun getQuotes(quotesRequest: QuotesRequest) {
            val call : Call<QuotesResponse> = RestApiClient.getService().getQuotes(
                    quotesRequest.page,
                    quotesRequest.perPage,
                    quotesRequest.query)
            call.enqueue(quotesRequest.callback)
        }

        fun getQuotes(categoryQuotesRequest: CategoryQuotesRequest) {
            val call : Call<QuotesResponse> = RestApiClient.getService().getQuotes(
                    categoryQuotesRequest.page,
                    categoryQuotesRequest.perPage,
                    categoryQuotesRequest.query,
                    categoryQuotesRequest.categoryId)
            call.enqueue(categoryQuotesRequest.callback)
        }

        fun createQuote(createQuoteRequest: CreateQuoteRequest) {
            val call : Call<ResponseBody> = RestApiClient.getService().createQuote(
                    createQuoteRequest)
            call.enqueue(createQuoteRequest.callback)
        }

        fun deleteQuote(id : String,
                        callback : BaseCallback<ResponseBody>) {
            val call : Call<ResponseBody> = RestApiClient.getService().deleteQuote(
                    id)
            call.enqueue(callback)
        }

        fun getAuthorQuotes(authorQuotesRequest: AuthorQuotesRequest) {
            val call : Call<QuotesResponse> = RestApiClient.getService().getAuthorQuotes(
                    authorQuotesRequest.authorId,
                    authorQuotesRequest.page,
                    authorQuotesRequest.perPage,
                    authorQuotesRequest.query)
            call.enqueue(authorQuotesRequest.callback)
        }

        fun getMyQuotes(quotesRequest: QuotesRequest) {
            val call : Call<QuotesResponse> = RestApiClient.getService().getMyQuotes()
            call.enqueue(quotesRequest.callback)
        }

        fun login(loginRequest: LoginRequest) {
            val call : Call<User> = RestApiClient.getService().login(loginRequest)
            call.enqueue(loginRequest.callback)
        }

        fun register(registerRequest: RegisterRequest) {
            val call : Call<ResponseBody> = RestApiClient.getService().register(registerRequest)
            call.enqueue(registerRequest.callback)
        }

        fun resetPassword(resetPasswordRequest: ResetPasswordRequest) {
            val call : Call<ResponseBody> = RestApiClient.getService().resetPassword(resetPasswordRequest)
            call.enqueue(resetPasswordRequest.callback)
        }

        fun getNotification(token : String?,
                            categories : ArrayList<String>) {
            val call : Call<ResponseBody> =
                    RestApiClient.getService().getNotification(
                            NotificationRequest(token, categories))
            call.enqueue(object : Callback<ResponseBody> {
                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                }

                override fun onResponse(call: Call<ResponseBody>, response: retrofit2.Response<ResponseBody>) {
                }

            })
        }
    }
}