package com.goodwill.tristan.emotivate.data.api

import com.google.gson.GsonBuilder
import com.goodwill.tristan.emotivate.BuildConfig
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by Azra on 1.8.2019.
 */

class RestApiClient {
    companion object {
        var mRetrofitClient : Retrofit? = null
        var mService : EndPoints? = null

        fun getRetrofitClient(baseUrl : String) : Retrofit? {
            if (mRetrofitClient == null) {
                val okHttpClient = OkHttpClient().newBuilder()
                val builder = GsonBuilder()
                val logger = HttpLoggingInterceptor()
                logger.setLevel(HttpLoggingInterceptor.Level.BODY)
                okHttpClient.interceptors().add(HeaderInterceptor())
                okHttpClient.interceptors().add(logger)
                okHttpClient.readTimeout(60, TimeUnit.SECONDS)
                mRetrofitClient = Retrofit.Builder().baseUrl(baseUrl).
                        client(okHttpClient.build()).
                        addConverterFactory(GsonConverterFactory.create(builder.create())).build()
            }
            return mRetrofitClient
        }

        fun getService() : EndPoints {
            if (mService == null) {
                throw IllegalStateException()
            }
            return mService!!
        }

        fun init() {
            mService = getRetrofitClient(BuildConfig.BASE_URL)?.create(EndPoints::class.java)
        }
    }
}