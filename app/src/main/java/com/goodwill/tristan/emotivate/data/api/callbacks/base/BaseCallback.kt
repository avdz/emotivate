package com.goodwill.tristan.emotivate.data.api.callbacks.base

import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-08-23
 **/
open class BaseCallback<T>(private val mResponseListener: ResponseListener<T>) : Callback<T> {

    override fun onFailure(call: Call<T>, t: Throwable) {
        throw t
    }

    override fun onResponse(call: Call<T>, response: Response<T>) {
        if (response.isSuccessful) {
            response.body()?.let { mResponseListener.onSuccess(it) }
        } else {
            mResponseListener.onError(response.errorBody())
        }
    }

}