package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import com.google.gson.annotations.SerializedName

/**
 *
 *
 *
 * @author Comp at 23.11.2019.
 **/

class CategoryQuotesRequest(val page : Int?,
                            val perPage : Int?,
                            val query : String?,
                            @SerializedName("category_id") val categoryId : Int?,
                            quotesCallback : BaseCallback<QuotesResponse>) :
        BaseRequest<QuotesResponse>(quotesCallback)