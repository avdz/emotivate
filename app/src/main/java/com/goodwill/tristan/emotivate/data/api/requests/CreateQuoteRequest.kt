package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 9.11.2019.
 **/

class CreateQuoteRequest(val description : String,
                         quotesCallback : BaseCallback<ResponseBody>) :
        BaseRequest<ResponseBody>(quotesCallback)