package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.goodwill.tristan.emotivate.data.api.response.AuthorsResponse
import com.goodwill.tristan.emotivate.models.Author

/**
 *
 *
 *
 * @author Comp at 20.11.2019.
 **/

class GetAuthorsRequest(val page: Int?,
                        val perPage: Int?,
                        val query : String?,
                        authorsCallback : BaseCallback<AuthorsResponse>) :
        BaseRequest<AuthorsResponse>(authorsCallback)