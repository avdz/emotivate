package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import com.goodwill.tristan.emotivate.models.User

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/
class LoginRequest(var username : String?,
                   var password : String?,
                   loginCallback : BaseCallback<User>) :
        BaseRequest<User>(loginCallback)