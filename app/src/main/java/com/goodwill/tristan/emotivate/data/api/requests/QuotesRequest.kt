package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-08-26
 **/
class QuotesRequest(val page : Int?,
                    val perPage : Int?,
                    val query : String?,
                    quotesCallback : BaseCallback<QuotesResponse>) :
        BaseRequest<QuotesResponse>(quotesCallback) {
    constructor(quotesCallback: BaseCallback<QuotesResponse>) :
            this(null, null, null, quotesCallback)
}