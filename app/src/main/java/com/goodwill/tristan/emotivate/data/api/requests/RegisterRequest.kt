package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.goodwill.tristan.emotivate.models.User
import com.google.gson.annotations.SerializedName
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 21.10.2019.
 **/

class RegisterRequest(var username : String?,
                      var nickname : String?,
                      var password : String?,
                      @SerializedName("confirm_password") var confirmPassword : String?,
                      loginCallback : BaseCallback<ResponseBody>) :
        BaseRequest<ResponseBody>(loginCallback)