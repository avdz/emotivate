package com.goodwill.tristan.emotivate.data.api.requests

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.base.BaseRequest
import com.google.gson.annotations.SerializedName
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 9.11.2019.
 **/

class ResetPasswordRequest(var username : String?,
                           var password : String?,
                           @SerializedName("confirm_password") var confirmPassword : String?,
                           loginCallback : BaseCallback<ResponseBody>) :
        BaseRequest<ResponseBody>(loginCallback)