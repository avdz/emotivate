package com.goodwill.tristan.emotivate.data.api.requests.base

import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-08-26
 **/
abstract class BaseRequest<T>(val callback : BaseCallback<T>)