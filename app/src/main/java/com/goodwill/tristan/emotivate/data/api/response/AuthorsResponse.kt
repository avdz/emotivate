package com.goodwill.tristan.emotivate.data.api.response

import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.Quote
import com.google.gson.annotations.SerializedName

/**
 *
 *
 *
 * @author Comp at 20.11.2019.
 **/

class AuthorsResponse(val pages : Int,
                      val total : Int,
                      val page : Int,
                      @SerializedName("has_next")
                      val hasNext : Boolean,
                      val authors : ArrayList<Author>)