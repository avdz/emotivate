package com.goodwill.tristan.emotivate.data.api.response

import com.google.gson.annotations.SerializedName
import com.goodwill.tristan.emotivate.models.Quote

data class QuotesResponse(val pages : Int,
                          val total : Int,
                          val page : Int,
                          @SerializedName("has_next")
                          val hasNext : Boolean,
                          val quotes : ArrayList<Quote>)