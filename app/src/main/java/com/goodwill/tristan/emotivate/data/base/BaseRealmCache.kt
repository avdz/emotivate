package com.goodwill.tristan.emotivate.data.base

import io.realm.Realm
import io.realm.RealmObject

/**
 * Notes:
 *
 *
 * //Write notes here
 *
 * ------------------------------------------------------------------
 *
 * @author: Džemal Ibrić
 * 14/09/2018
 * <dzemal.ibric></dzemal.ibric>@klika.ba>
 */
open class BaseRealmCache<T : RealmObject> (val entityClass: Class<T>) {
    /**
     * Initialize with entity class
     */
    /**
     * Load all object from database
     *
     * @param realm - realm instance
     * @return List of objects of entityClass types
     */
    fun load(realm: Realm): List<T> {
        return realm.where(entityClass).findAll()
    }

    /**
     * Initialize with entity class
     */
    /**
     * Load all object from database with limit
     *
     * @param realm - realm instance
     * @return List of objects of entityClass types
     */
    fun loadWithLimit(realm: Realm, limit : Long): List<T> {
        return realm.where(entityClass).limit(limit).findAll()
    }

    /**
     * Delete all and insert new object
     *
     * @param realm   realm instance
     * @param objects list of objects (extended from RealmObject)
     */
    fun cleanSave(realm: Realm, objects: List<*>) {
        deleteAll(realm)

        realm.beginTransaction()
        for (realmObject in objects) {
            realm.copyToRealm(realmObject as RealmObject)
        }

        realm.commitTransaction()
    }

    fun save(realm: Realm, objects: List<*>?) {

        realm.beginTransaction()

        if (objects != null) {
            for (realmObject in objects) {
                realm.copyToRealm(realmObject as RealmObject)
            }
        }

        realm.commitTransaction()
    }

    /**
     * Update or create object if needed. object must have PrimaryKey
     *
     * @param realm  realm instance
     * @param object object to update/create (etended from RealmObject)
     */
    fun updateOrCreate(realm: Realm, `object`: T) {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(`object`)
        realm.commitTransaction()
    }

    /**
     * Update or create objects if needed. Object must have PrimaryKey.
     *
     * @param realm           realm instance
     * @param objects         objects to update/create (extended from RealmObject)
     * @param deleteIfMissing deletes objects which are not in th list
     */
    fun updateOrCreate(realm: Realm, objects: List<RealmObject>?, deleteIfMissing: Boolean) {
        if (objects?.isEmpty()!!) {
            return
        }
        val clazz = objects[0].javaClass
        realm.beginTransaction()
        val liveObjects = realm.copyToRealmOrUpdate(objects)

        // If true, all objects in DB which are not updated or added will be deleted
        if (deleteIfMissing) {
            val allDbObjects = realm.where(clazz).findAll()
            for (realmObject in allDbObjects) {
                if (!liveObjects.contains(realmObject)) {
                    realmObject.deleteFromRealm()
                }
            }
        }
        realm.commitTransaction()
    }

    /**
     * Delete all models from realm
     *
     * @param realm realm instance
     */
    fun deleteAll(realm: Realm) {
        realm.beginTransaction()
        realm.delete(entityClass)
        realm.commitTransaction()
    }

    fun <E : RealmObject> copyFromRealm(realm: Realm, realmObject: E?): E? {
        return if (realmObject != null) {
            realm.copyFromRealm(realmObject)
        } else {
            null
        }
    }

}
