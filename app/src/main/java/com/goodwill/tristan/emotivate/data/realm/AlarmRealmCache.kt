package com.goodwill.tristan.emotivate.data.realm

import com.goodwill.tristan.emotivate.data.base.BaseRealmCache
import com.goodwill.tristan.emotivate.models.AlarmCacheModel
import io.realm.Realm

/**
 * Created by Azra on 13.5.2019.
 */
open class AlarmRealmCache : BaseRealmCache<AlarmCacheModel>(AlarmCacheModel::class.java) {

    fun saveAlarms(alarmModel: AlarmCacheModel) {
        Realm.getDefaultInstance().use { realm -> updateOrCreate(realm, alarmModel) }
    }

    fun getAlarms() : AlarmCacheModel? {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).findFirst()) }
    }

    fun hasAlarms(): Boolean {
        Realm.getDefaultInstance().use { realm -> return copyFromRealm(
                realm, realm.where(entityClass).findFirst()) != null }
    }
}