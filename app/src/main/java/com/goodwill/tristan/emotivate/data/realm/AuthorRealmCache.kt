package com.goodwill.tristan.emotivate.data.realm

import com.goodwill.tristan.emotivate.data.base.BaseRealmCache
import com.goodwill.tristan.emotivate.models.Author
import io.realm.Realm

open class AuthorRealmCache : BaseRealmCache<Author>(Author::class.java) {

    fun saveAuthors(authors: ArrayList<Author>?) {
        Realm.getDefaultInstance().use { realm ->
            updateOrCreate(realm, authors, true)
        }
    }

    fun getAuthors(): List<Author>? {
        Realm.getDefaultInstance().use { realm ->
            return load(realm)
        }
    }
}