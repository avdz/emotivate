package com.goodwill.tristan.emotivate.data.realm

import com.goodwill.tristan.emotivate.data.base.BaseRealmCache
import com.goodwill.tristan.emotivate.models.Category
import io.realm.Realm



open class CategoryRealmCache : BaseRealmCache<Category>(Category::class.java) {

    fun saveCategories(categories: ArrayList<Category>?) {
        val realm = Realm.getDefaultInstance()
        saveChangedCategories(realm, categories)
    }

    fun getCategories(): List<Category>? {
        val realm = Realm.getDefaultInstance()
        return load(realm)
    }

    fun saveCategory(category: Category) {
        val realm = Realm.getDefaultInstance()
        updateOrCreate(realm, category)
    }

    fun delete(id : Int) {
        Realm.getDefaultInstance().executeTransaction { realm ->
            val rows = realm.where(entityClass).equalTo("id", id).findAll()
            rows.deleteAllFromRealm()
        }
    }

    fun getVisibleCategoryIdsString(): ArrayList<String> {
        val realm = Realm.getDefaultInstance()
        val categories = realm.where(entityClass)
                .equalTo("isVisible", true)
                .findAll()
        val categoryIds = ArrayList<String>()
        for (category in categories) {
            categoryIds.add(category.id.toString())
        }
        return categoryIds
    }

    fun setCategoryVisible(categoryId : Int,
                           name : String,
                           isVisible : Boolean) {
        val realm = Realm.getDefaultInstance()
        val category = Category(categoryId, name, isVisible)
        updateOrCreate(realm, category)
    }

    fun getCategory(id : Int?) : Category? {
        val realm = Realm.getDefaultInstance()
        return realm.where(entityClass).equalTo("id", id).findFirst()
    }

    private fun saveChangedCategories(realm: Realm, newCategories: List<Category>?) {

        val existingCategories = ArrayList(load(realm))
        if (existingCategories.isNullOrEmpty()) {
            save(realm, newCategories)
            return
        }
        realm.beginTransaction()
        if (newCategories != null) {
            for (newCategory in newCategories) {
                realm.copyToRealmOrUpdate(
                        generateChangedCategory(newCategory, existingCategories))
            }
        }
        realm.commitTransaction()
    }

    private fun generateChangedCategory(category : Category,
                                        categories : ArrayList<Category>) : Category? {
        if (categories.contains(category)) {
            for (existingCategory in categories) {
                if (category.id == existingCategory.id) {
                    category.isVisible = existingCategory.isVisible
                    return category
                }
            }
            return null
        } else {
            return category
        }
    }
}