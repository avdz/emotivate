package com.goodwill.tristan.emotivate.data.realm

import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.MenuListModel

/**
 * Created by Azra on 12.5.2019.
 */
class MenuRealmCache {

    fun getMenuListModelsFromCache() : ArrayList<MenuListModel> {
        val menuListModels = ArrayList<MenuListModel>()
        val categories = ArrayList(CategoryRealmCache().getCategories())
        for (category : Category in categories) {
            menuListModels.add(generateMenuListModel(
                    category.id,
                    category.name,
                    category.isVisible))
        }
        return menuListModels
    }

    private fun generateMenuListModel(id : Int,
                                      name : String,
                                      checked : Boolean) : MenuListModel {
        return MenuListModel(id, name, checked)
    }
}