package com.goodwill.tristan.emotivate.data.shared_preferences

import com.goodwill.tristan.emotivate.data.shared_preferences.base.EmotivateSharedPreferences

/**
 * Notes:
 *
 *
 *
 * @author dzemal.ibric at 2019-11-01
 **/
class NotificationsStorage : EmotivateSharedPreferences() {

    companion object {
        val NOTIFICATION_SOUND = "notification_sound"

        fun setNotificationsSoundEnabled(soundEnabled : Boolean) {
            put(NOTIFICATION_SOUND, soundEnabled)
        }

        fun isSoundEnabled() : Boolean {
            return get<Boolean>(NOTIFICATION_SOUND) ?: false
        }
    }
}