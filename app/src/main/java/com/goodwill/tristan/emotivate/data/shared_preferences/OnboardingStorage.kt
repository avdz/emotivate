package com.goodwill.tristan.emotivate.data.shared_preferences

import com.goodwill.tristan.emotivate.data.shared_preferences.base.EmotivateSharedPreferences

/**
 * Notes:
 *
 *
 *
 * @author dzemal.ibric at 2019-11-01
 **/
class OnboardingStorage : EmotivateSharedPreferences() {

    companion object {
        val ONBOARDING_COMPLETED = "onboarding_completed"

        fun setOnboardingCompleted() {
            put(ONBOARDING_COMPLETED, true)
        }

        fun isOnboardingCompleted() : Boolean {
            return get<Boolean>(ONBOARDING_COMPLETED) ?: false
        }
    }
}