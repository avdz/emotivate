package com.goodwill.tristan.emotivate.data.shared_preferences

import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.data.shared_preferences.base.EmotivateSharedPreferences
import com.goodwill.tristan.emotivate.models.User

/**
 * Notes:
 *
 *
 *
 * @author dzemal.ibric at 2019-11-01
 **/
class UserStorage : EmotivateSharedPreferences() {

    companion object {
        val USER = "user"

        fun getUser() : User? {
            return get<User>(USER)
        }

        fun saveUser(user : User) {
            put(USER, user)
        }

        fun logOut() {
            CategoryRealmCache().delete(0)
            delete(USER)
        }

        fun hasUser() : Boolean {
            return getUser() != null
        }

    }

}