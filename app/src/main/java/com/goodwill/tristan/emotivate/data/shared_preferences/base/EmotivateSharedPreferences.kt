package com.goodwill.tristan.emotivate.data.shared_preferences.base

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import java.lang.Exception


/**
 * Created by Azra on 16.5.2019.
 */
abstract class EmotivateSharedPreferences {

    companion object {
        private val TAG = "emotivate_shared_preferences"
        protected var mSharedPreferences : SharedPreferences? = null

        fun init(context: Context) {
            mSharedPreferences = context.getSharedPreferences(TAG, Context.MODE_PRIVATE)
        }

        fun <T> put(key: String, value: T) {
            val gson = Gson()

            val editor = mSharedPreferences?.edit()
            editor?.putString(key, gson.toJson(value))
            editor?.apply()
        }

        inline fun <reified T> get(key : String) : T? {
            var savedObject : T? = null
            try {
                savedObject = Gson().fromJson<T>(
                        mSharedPreferences?.getString(key, null), T::class.java)
            } catch (e : Exception) {
                e.printStackTrace()
            }
            finally {
                return savedObject
            }
        }

        fun delete(key : String) {
            val editor = mSharedPreferences?.edit()
            editor?.remove(key)
            editor?.apply()
        }

    }
}