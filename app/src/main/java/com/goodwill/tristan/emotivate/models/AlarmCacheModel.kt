package com.goodwill.tristan.emotivate.models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Created by Azra on 13.5.2019.
 */
open class AlarmCacheModel(@PrimaryKey var id: Int = 0,
                           var alarmModels: RealmList<AlarmModel> = RealmList()) : RealmObject() {

    fun getAlarmModels() : ArrayList<AlarmModel> {
        val alarmListModel = ArrayList<AlarmModel>()
        alarmListModel += alarmModels

        return alarmListModel
    }
}