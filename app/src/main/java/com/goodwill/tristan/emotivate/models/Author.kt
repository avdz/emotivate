package com.goodwill.tristan.emotivate.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Author(@PrimaryKey var id : Int = 0,
                  var name : String = "",
                  var description : String = "",
                  @SerializedName("image_url") var imageUrl : String = "") :
        RealmObject()