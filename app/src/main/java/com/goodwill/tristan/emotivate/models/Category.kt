package com.goodwill.tristan.emotivate.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class Category(@PrimaryKey var id : Int = 0,
                    var name : String = "",
                    var isVisible : Boolean = true) : RealmObject()