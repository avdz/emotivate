package com.goodwill.tristan.emotivate.models

import android.graphics.Bitmap

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-09
 **/
class CategoryImage(val categoryId : Int,
                    val image : Bitmap?)