package com.goodwill.tristan.emotivate.models

/**
 * Created by Azra on 7.8.2018.
 */
class MenuListModel(var modelId : Int,
                    var modelText : String,
                    var modelVisible : Boolean)