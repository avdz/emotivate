package com.goodwill.tristan.emotivate.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 27/03/2019
 * <dzemal.ibric@klika.ba>
 */
open class Quote(@PrimaryKey var id : Int = 0,
                 var description : String = "",
                 var categoryId : Int = 0,
                 var authorId : Int = 0,
                 var author : Author? = null,
                 var category : Category? = null) : RealmObject()