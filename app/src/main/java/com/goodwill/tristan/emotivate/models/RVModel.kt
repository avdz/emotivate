package com.goodwill.tristan.emotivate.models

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-09-03
 **/
open class RVModel(var mViewType : ViewType) {

    enum class ViewType {
        QUOTE,
        AUTHOR,
        PROGRESS,
        FAVORITE_QUOTE,
        CATEGORY;

        companion object {
            fun getViewType(position: Int): ViewType {
                return values()[position]
            }
        }
    }
}