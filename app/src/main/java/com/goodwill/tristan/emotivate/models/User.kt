package com.goodwill.tristan.emotivate.models

import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

open class User(@PrimaryKey var id : Int = 0,
                var username : String = "",
                var nickname : String? = "",
                @SerializedName("access_token") var accessToken : String = "",
                var limit : Int = 0,
                var quotes : RealmList<Int>? = RealmList(),
                var admin : Boolean = false) : RealmObject()