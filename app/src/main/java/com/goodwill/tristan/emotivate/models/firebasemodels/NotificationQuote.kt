package com.goodwill.tristan.emotivate.models.firebasemodels

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-07
 **/
data class NotificationQuote(val notificationTitle : String?,
                             val notificationText : String?,
                             val notificationAuthor : String?)