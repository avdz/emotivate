package com.goodwill.tristan.emotivate.models.firebasemodels

import com.google.gson.annotations.SerializedName

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-07
 **/
data class NotificationRequest(val token : String?,
                               @SerializedName("category_ids")
                               val categories : ArrayList<String>)