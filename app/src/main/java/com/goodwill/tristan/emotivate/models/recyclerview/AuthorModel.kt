package com.goodwill.tristan.emotivate.models.recyclerview

import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.utilities.StringUtilities

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-09
 **/
data class AuthorModel(val author : Author,
                       var highlightedText : String? = null) : RVModel(ViewType.AUTHOR) {
    fun containsString(string : String?) : Boolean {
        return string != null && StringUtilities.containsIgnoreCase(author.name, string)
    }
}