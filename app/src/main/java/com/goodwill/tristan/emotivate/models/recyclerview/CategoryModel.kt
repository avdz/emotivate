package com.goodwill.tristan.emotivate.models.recyclerview

import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.RVModel

/**
 *
 *
 *
 * @author Comp at 23.11.2019.
 **/

class CategoryModel(val category : Category,
                    var isActive : Boolean) : RVModel(ViewType.CATEGORY)