package com.goodwill.tristan.emotivate.models.recyclerview

import com.goodwill.tristan.emotivate.models.RVModel

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-09-03
 **/
class LoadMoreModel : RVModel(ViewType.PROGRESS)