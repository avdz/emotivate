package com.goodwill.tristan.emotivate.models.recyclerview

import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.utilities.StringUtilities

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-09-03
 **/
class QuoteModel(val quote : Quote,
                 val viewType : ViewType = ViewType.QUOTE,
                 var highlightedText: String? = null) : RVModel(viewType) {

    fun containsString(string : String?) : Boolean {
        return string != null && StringUtilities.containsIgnoreCase(quote.description, string)
    }
}