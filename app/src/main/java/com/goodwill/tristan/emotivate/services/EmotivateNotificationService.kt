package com.goodwill.tristan.emotivate.services

import android.util.Log
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.models.firebasemodels.NotificationQuote
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage



/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-07
 **/
class EmotivateNotificationService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        val notificationTitle = remoteMessage.data["title"]
        val notificationText = remoteMessage.data["text"]
        val notificationAuthor = remoteMessage.data["author"]
        if (remoteMessage.data.isNotEmpty()) {
            NotificationPusherService(applicationContext)
                    .notify(NotificationQuote(
                            notificationTitle, notificationText, notificationAuthor))
        }
    }
}