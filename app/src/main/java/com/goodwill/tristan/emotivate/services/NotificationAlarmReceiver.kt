package com.goodwill.tristan.emotivate.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId


/**
 * Created by Comp on 14.5.2019..
 */
class NotificationAlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (context != null) {
            createNotification()
        }
    }

    companion object {
        fun createNotification() {
            FirebaseInstanceId.getInstance().instanceId
                    .addOnCompleteListener(OnCompleteListener { task ->
                        if (!task.isSuccessful) {
                            return@OnCompleteListener
                        }
                        // Get new Instance ID token
                        val token = task.result?.token
                        HttpService.getNotification(token, getVisibleCategories())
                    })
        }

        private fun getVisibleCategories() : ArrayList<String> {
            return CategoryRealmCache().getVisibleCategoryIdsString()
        }
    }
}