package com.goodwill.tristan.emotivate.services

import android.app.Notification
import android.app.PendingIntent
import android.app.TaskStackBuilder
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.StyleSpan
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.shared_preferences.NotificationsStorage
import com.goodwill.tristan.emotivate.models.firebasemodels.NotificationQuote
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.utilities.StringUtilities

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 22/10/2018
 * <dzemal.ibric@klika.ba>
 */
class NotificationPusherService(private val mContext: Context) {

    val NOTIFICATION_ID = "NotificationId"

    fun notify(notificationQuote: NotificationQuote?) {
        with(NotificationManagerCompat.from(mContext)) {
            // notificationId is a unique int for each notification that you must define
            notify(1, createNotification(notificationQuote))
        }
    }

    private fun createNotification(notificationQuote: NotificationQuote?) : Notification {
        val mBuilder = NotificationCompat.Builder(mContext, NOTIFICATION_ID)
                .setSmallIcon(R.mipmap.ic_logo_black)
                .setContentTitle(notificationQuote?.notificationTitle)
                .setContentText(generateNotificationText(
                        notificationQuote?.notificationText,
                        notificationQuote?.notificationAuthor))
                .setLargeIcon(BitmapFactory.decodeResource(
                        mContext.resources,
                        R.mipmap.ic_logo_green))
                .setColor(Color.WHITE)
                .setStyle(NotificationCompat.BigTextStyle()
                        .bigText(generateNotificationText(
                                notificationQuote?.notificationText,
                                notificationQuote?.notificationAuthor)))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)
                .setContentIntent(createPendingIntent())
                .setChannelId(mContext.getString(R.string.emotivate_channel_id))

        if (NotificationsStorage.isSoundEnabled()) {
            mBuilder.setVibrate(listOf<Long>(0, 200, 200, 200).toLongArray())
        }
        return mBuilder.build()
    }

    private fun createPendingIntent(): PendingIntent? {
        val resultIntent = Intent(mContext, EmotivateMainActivity::class.java)
        return TaskStackBuilder.create(mContext).run {
            // Add the intent, which inflates the back stack
            addNextIntentWithParentStack(resultIntent)
            // Get the PendingIntent containing the entire back stack
            getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT)
        }
    }

    private fun generateNotificationText(quoteDescription : String?,
                                         quoteAuthor : String?): SpannableString {
        val description = StringUtilities.returnStringInQuotes(quoteDescription)

        val author: String = "\n" + quoteAuthor
        val spannableString = SpannableString(description + author)
        spannableString.setSpan(
                StyleSpan(Typeface.ITALIC),
                description.length,
                description.length + author.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
        return spannableString

    }
}