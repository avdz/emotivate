package com.goodwill.tristan.emotivate.ui.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.models.recyclerview.AuthorModel
import com.goodwill.tristan.emotivate.models.recyclerview.CategoryModel
import com.goodwill.tristan.emotivate.models.recyclerview.LoadMoreModel
import com.goodwill.tristan.emotivate.models.recyclerview.QuoteModel
import com.goodwill.tristan.emotivate.ui.adapters.base.BaseRVAdapter
import com.goodwill.tristan.emotivate.ui.viewholders.*
import com.goodwill.tristan.emotivate.utilities.interfaces.DeleteListener
import com.goodwill.tristan.emotivate.utilities.interfaces.OnItemInteractedListener

/**
 *
 *
 *
 * @author Comp at 23.11.2019.
 **/

class CategoriesRVAdapter : BaseRVAdapter() {

    private var mListener : OnItemInteractedListener? = null

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        (viewHolder as CategoryItemViewHolder).bind(mItems[position] as CategoryModel, position, mListener!!)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return CategoryItemViewHolder.getViewHolder(parent)
    }

    fun setClickListener(itemInteractedListener: OnItemInteractedListener) {
        mListener = itemInteractedListener
    }

    fun itemSelected(index : Int) {
        (mItems[index] as CategoryModel).isActive = !(mItems[index] as CategoryModel).isActive
        for (itemIndex in mItems.indices) {
            if (itemIndex != index) {
                (mItems[itemIndex] as CategoryModel).isActive = false
            }
        }
        notifyDataSetChanged()
    }

    fun getSelectedCategory() : Int? {
        for (item in mItems) {
            val categoryModel = item as CategoryModel
            if (categoryModel.isActive) {
                return categoryModel.category.id
            }
        }
        return null
    }
}