package com.goodwill.tristan.emotivate.ui.adapters

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.models.recyclerview.AuthorModel
import com.goodwill.tristan.emotivate.models.recyclerview.LoadMoreModel
import com.goodwill.tristan.emotivate.models.recyclerview.QuoteModel
import com.goodwill.tristan.emotivate.ui.adapters.base.BaseRVAdapter
import com.goodwill.tristan.emotivate.ui.viewholders.AuthorItemViewHolder
import com.goodwill.tristan.emotivate.ui.viewholders.FavoritesItemViewHolder
import com.goodwill.tristan.emotivate.ui.viewholders.LoadMoreItemViewHolder
import com.goodwill.tristan.emotivate.ui.viewholders.QuoteItemViewHolder
import com.goodwill.tristan.emotivate.utilities.interfaces.DeleteListener

/**
 * Created by Comp on 27.9.2018..
 */
class EmotivateRVAdapter : BaseRVAdapter() {

    private var mDeleteListener : DeleteListener? = null

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        when ((mItems[position] as RVModel).mViewType) {
            RVModel.ViewType.QUOTE -> {
                (viewHolder as QuoteItemViewHolder).bind(mItems[position] as QuoteModel, position)
            }

            RVModel.ViewType.PROGRESS -> {
                (viewHolder as LoadMoreItemViewHolder).bind(mItems[position] as LoadMoreModel, position)
            }
            RVModel.ViewType.AUTHOR -> {
                (viewHolder as AuthorItemViewHolder).bind(mItems[position] as AuthorModel, position)
            }
            RVModel.ViewType.FAVORITE_QUOTE -> {
                (viewHolder as FavoritesItemViewHolder).bind(
                        mItems[position] as QuoteModel, position, mDeleteListener)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(RVModel.ViewType.getViewType(viewType)) {
            RVModel.ViewType.QUOTE -> {
                QuoteItemViewHolder.getViewHolder(parent)
            }
            RVModel.ViewType.PROGRESS -> {
                LoadMoreItemViewHolder.getViewHolder(parent)
            }
            RVModel.ViewType.AUTHOR -> {
                AuthorItemViewHolder.getViewHolder(parent)
            }
            RVModel.ViewType.FAVORITE_QUOTE -> {
                FavoritesItemViewHolder.getViewHolder(parent)
            }
            else -> return QuoteItemViewHolder.getViewHolder(parent)
        }
    }

    fun setDeleteListener(deleteListener: DeleteListener) {
        mDeleteListener = deleteListener
    }

    fun getDeleteListener() : DeleteListener? {
        return mDeleteListener
    }

    fun setHighlightedText(highlightedText: String?) {
        for (index in 0 until mItems.size) {
            if (mItems[index] is QuoteModel) {
                val quote = mItems[index] as QuoteModel
                if (highlightedText != null && quote.containsString(highlightedText)) {
                    updateItem(quote, index)
                    quote.highlightedText = highlightedText
                }
            } else if (mItems[index] is AuthorModel){
                val author = mItems[index] as AuthorModel
                if (highlightedText != null && author.containsString(highlightedText)) {
                    updateItem(author, index)
                    author.highlightedText = highlightedText
                }
            } else {
                return
            }
        }
    }

}