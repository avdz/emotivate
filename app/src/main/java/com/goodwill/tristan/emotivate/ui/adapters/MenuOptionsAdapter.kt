package com.goodwill.tristan.emotivate.ui.adapters

import android.content.Context
import android.view.View
import android.widget.CheckBox
import android.widget.CompoundButton
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.MenuListModel
import com.goodwill.tristan.emotivate.ui.adapters.base.BaseListAdapter
import com.goodwill.tristan.emotivate.ui.widgets.EmotivateTextView

/**
 * Created by Azra on 7.8.2018.
 */
class MenuOptionsAdapter(mContext: Context?,
                         var menuListModels: ArrayList<MenuListModel>) : BaseListAdapter<MenuListModel>(mContext, menuListModels) {

    override fun setupView(position: Int, view: View?): View? {
        val checkBox = view?.findViewById<CheckBox>(R.id.optionsCheck)
        val text = view?.findViewById<EmotivateTextView>(R.id.optionsText)
        val menuListModel = menuListModels[position]
        checkBox?.setOnCheckedChangeListener { compoundButton: CompoundButton, isChecked: Boolean ->
            menuListModels[position].modelVisible = isChecked
        }
        checkBox?.isChecked = menuListModel.modelVisible
        text?.text = menuListModel.modelText

        return view!!
    }

    override val layoutResourceId: Int
        get() = R.layout.menu_row




}