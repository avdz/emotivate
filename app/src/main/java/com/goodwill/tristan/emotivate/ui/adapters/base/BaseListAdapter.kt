package com.goodwill.tristan.emotivate.ui.adapters.base

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter

/**
 * Created by Azra on 15.5.2019.
 */
abstract class BaseListAdapter<T>(val mContext : Context?,
                                  var mModels : ArrayList<T>) : BaseAdapter() {

    abstract val layoutResourceId : Int

    abstract fun setupView(position: Int, view : View?) : View?

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) {
            view = LayoutInflater.from(mContext).inflate(
                    layoutResourceId,
                    parent,
                    false)
        }
        view = setupView(position, view)
        return view!!
    }

    override fun getCount(): Int {
        return mModels.size
    }

    override fun getItem(position: Int): Any {
        return mModels[position] as Any
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }
}