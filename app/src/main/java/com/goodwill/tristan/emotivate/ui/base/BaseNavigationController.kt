package com.goodwill.tristan.emotivate.ui.base

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.goodwill.tristan.emotivate.ui.base.activity.EmotivateBaseActivity


/**
 * Created by Comp on 25.7.2018..
 */
open class BaseNavigationController(var baseActivity: EmotivateBaseActivity<*>) {

    private var fragmentManager : FragmentManager? = null

    init {
        this.fragmentManager = baseActivity.supportFragmentManager
    }

    protected fun addFragment(fragment: Fragment, tag: String) {
        fragmentManager?.beginTransaction()
                ?.replace(baseActivity.getFragmentContainerId(), fragment, tag)
                ?.commitAllowingStateLoss()
    }

    protected fun addFragmentWithBackstack(fragment: Fragment, tag: String) {
        fragmentManager?.beginTransaction()
                ?.replace(baseActivity.getFragmentContainerId(), fragment, tag)
                ?.addToBackStack(null)
                ?.commitAllowingStateLoss()
    }

    fun addFragmentWithBackstack(fragment: Fragment) {
        fragmentManager?.beginTransaction()
                ?.replace(baseActivity.getFragmentContainerId(), fragment)
                ?.addToBackStack(fragment.tag)
                ?.setReorderingAllowed(true)
                ?.commitAllowingStateLoss()
    }

    protected fun addOrChangeFragment(fragment: Fragment, tag: String) {
        if (getFragmentByTag(tag) != null) {
            addFragment(fragmentManager?.findFragmentByTag(tag)!!, tag)
        } else {
            addFragment(fragment, tag)
        }
    }

    protected fun addOrChangeFragmentWithBackstack(fragment: Fragment, tag: String) {
        if (getFragmentByTag(tag) != null) {
            addFragmentWithBackstack(fragmentManager?.findFragmentByTag(tag)!!, tag)
        } else {
            addFragmentWithBackstack(fragment, tag)
        }
    }

    fun getFragmentByTag(tag: String) : Fragment? {
        return fragmentManager?.findFragmentByTag(tag)
    }
}