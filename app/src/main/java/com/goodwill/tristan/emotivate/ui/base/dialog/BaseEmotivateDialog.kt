package com.goodwill.tristan.emotivate.ui.base.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import com.goodwill.tristan.emotivate.ui.screen.main.dialog.ProgressDialog
import com.goodwill.tristan.emotivate.utilities.AppUtilities
import com.goodwill.tristan.emotivate.utilities.exceptions.ContextNotPresentException

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 02/04/2019
 * <dzemal.ibric@klika.ba>
 */
abstract class BaseEmotivateDialog(mContext : Context?) : Dialog(mContext!!) {
    init {
        if (mContext == null) {
            throw ContextNotPresentException()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(mLayoutRId)
        setupDialog()
    }

    abstract val mLayoutRId : Int

    open fun setupDialog() {}
}