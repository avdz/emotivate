package com.goodwill.tristan.emotivate.ui.base.dialog

import android.content.Context
import com.goodwill.tristan.emotivate.R
import kotlinx.android.synthetic.main.dialog_info.*

/**
 *
 *
 *
 * @author Comp at 21.10.2019.
 **/

class InfoDialog(context: Context?,
                 val type : InfoDialogType,
                 val title : String,
                 val text : String,
                 val dialogDismissListener: DialogDismissListener? = null) : BaseEmotivateDialog(context) {

    enum class InfoDialogType {
        ERROR,
        INFO,
        SUCCESS
    }

    init {
        window.run { this?.setBackgroundDrawableResource(android.R.color.transparent) }
        setCancelable(false)
    }

    override val mLayoutRId: Int
        get() = R.layout.dialog_info

    override fun setupDialog() {
        setupDialogType()
        infoText.text = text
        infoTitle.text = title
        infoButton.setOnClickListener {
            dialogDismissListener?.onDialogDismissed()
            dismiss()
        }
    }

    private fun setupDialogType() {
        when (type) {
            InfoDialogType.ERROR -> {
                infoHeaderImage.setImageDrawable(
                        context.resources.getDrawable(R.drawable.ic_error_popup))
                infoButton.background = context.resources.
                        getDrawable(R.drawable.error_button_background)
            }
            InfoDialogType.INFO -> {
                infoHeaderImage.setImageDrawable(
                        context.resources.getDrawable(R.drawable.ic_error_popup))
                infoButton.background = context.resources.
                        getDrawable(R.drawable.error_button_background)
            }
            InfoDialogType.SUCCESS -> {
                infoHeaderImage.setImageDrawable(
                        context.resources.getDrawable(R.drawable.ic_success_popup))
                infoButton.background = context.resources.
                        getDrawable(R.drawable.success_button_background)
            }
        }
    }

    interface DialogDismissListener {
        fun onDialogDismissed()
    }
}