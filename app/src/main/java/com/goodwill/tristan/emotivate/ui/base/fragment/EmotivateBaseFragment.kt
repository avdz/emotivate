package com.goodwill.tristan.emotivate.ui.base.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.goodwill.tristan.emotivate.ui.base.BaseNavigationController
import com.goodwill.tristan.emotivate.ui.base.activity.EmotivateBaseActivity
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.ui.screen.main.dialog.ProgressDialog
import com.goodwill.tristan.emotivate.utilities.AppUtilities
import com.goodwill.tristan.emotivate.utilities.managers.NavigationManager
import java.lang.Exception

/**
 * Created by Comp on 25.7.2018..
 */
abstract class EmotivateBaseFragment<NAV_CONTROLLER_TYPE : BaseNavigationController> : Fragment() {

    private var navigationController : NAV_CONTROLLER_TYPE? = null
    private var mProgressDialog : ProgressDialog? = null
    protected lateinit var  mNavigationManager : NavigationManager

    protected abstract val mLayoutRId : Int

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        if (activity is EmotivateBaseActivity<*>) {
            val navigationController = (activity as EmotivateBaseActivity<*>).getNavigationController()
            this.navigationController = navigationController as NAV_CONTROLLER_TYPE
        } else {
            throw RuntimeException("Activity has to extend BaseActivity to use navigation controllers")
        }
        mNavigationManager = context?.let { NavigationManager(it) }!!
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return LayoutInflater.from(context).inflate(mLayoutRId, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        initView(view)
    }

    override fun onDetach() {
        super.onDetach()
        this.navigationController = null
    }

    protected fun getNavigationController(): NAV_CONTROLLER_TYPE? {
        return navigationController
    }

    protected abstract fun initView(view: View)

    protected fun showProgressDialog() {
        if (!activity?.isFinishing!!) {
            if (mProgressDialog == null) {
                mProgressDialog = ProgressDialog(context!!)
                mProgressDialog!!.show()
            } else {
                mProgressDialog!!.show()
            }
        }
    }

    protected fun dismissProgressDialog() {
        if (mProgressDialog != null && mProgressDialog?.isShowing!!) {
            mProgressDialog?.dismiss()
        }
    }

    fun setToolbarTitle(title : String) {
        try {
            (activity as EmotivateMainActivity).setToolbarTitle(title)
        } catch (e : Exception) {

        }
    }


    protected fun finishParentActivity() {
        AppUtilities.getActivityFromContext(context)?.finish()
    }

}