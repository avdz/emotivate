package com.goodwill.tristan.emotivate.ui.base.fragment

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.models.recyclerview.LoadMoreModel
import com.goodwill.tristan.emotivate.ui.screen.main.MainNavigationController
import com.goodwill.tristan.emotivate.ui.adapters.EmotivateRVAdapter
import com.goodwill.tristan.emotivate.ui.widgets.SearchView
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-09
 **/
abstract class RecyclerViewFragment<MODEL> : EmotivateBaseFragment<MainNavigationController>(),
        SwipeRefreshLayout.OnRefreshListener,
        SearchView.SearchViewListener where MODEL : RVModel  {

    protected var mAdapter : EmotivateRVAdapter? = null
    protected val mProgress = LoadMoreModel()
    protected var page = 1
    protected var perPage = 10
    protected var hasNextPage = false
    protected var loadMoreVisible = true
    protected var mQuery : String? = null

    protected abstract val mAdView : AdView

    protected abstract val mSearchView : SearchView?

    protected abstract val mRecyclerView : RecyclerView?

    protected abstract val mSwipeRefresh : SwipeRefreshLayout

    protected abstract val mShouldLoadMore : Boolean

    override fun initView(view: View) {
        setupSwipeRefresh()
        mSearchView?.setSearchViewListener(this)
        createAdapter()
        getItems(1)
        mAdView.loadAd(AdRequest.Builder().build())
    }

    protected abstract fun getItems(page : Int)

    protected fun setupRecyclerView(models : ArrayList<MODEL>?) {
        if (page == 1 || page == 0) {
            createAdapter()
        }

        mAdapter?.insertItems(models)

        if (hasNextPage && !mAdapter?.contains(mProgress)!! && mAdapter?.itemCount!! > 0) {
            addLoadMore()
        }
    }

    private fun setupSwipeRefresh() {
        mSwipeRefresh.setOnRefreshListener(this)
        mSwipeRefresh.setColorSchemeColors(resources.getColor(R.color.status_bar_color))
    }

    override fun onRefresh() {
        page = 1
        startRefreshing()
        getItems(page)
    }

    protected open fun createAdapter() {
        mRecyclerView?.layoutManager = LinearLayoutManager(context)
        mAdapter = EmotivateRVAdapter()
        mRecyclerView?.adapter = mAdapter
        if (mShouldLoadMore) {
            mRecyclerView?.addOnScrollListener(
                    object : RecyclerView.OnScrollListener() {
                        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                            super.onScrollStateChanged(recyclerView, newState)
                            if (!recyclerView.canScrollVertically(1) && loadMoreVisible) {
                                onLoadMore()
                            }
                        }
                    })
        }
    }

    override fun onNewSearchQuery(query: String?) {
        mQuery = query
        onRefresh()
    }

    protected fun startRefreshing() {
        mSwipeRefresh.isRefreshing = true
    }

    protected fun stopRefreshing() {
        mSwipeRefresh.isRefreshing = false
    }

    private fun onLoadMore() {
        removeLoadMore()
        if (mAdapter == null) {
            mAdapter = EmotivateRVAdapter()
        }
        val nextPage = if (hasNextPage) page + 1 else page
        getItems(nextPage)
        loadMoreVisible = false
    }

    private fun removeLoadMore() {
        if (mAdapter != null && mAdapter!!.itemCount != 0) {
            mAdapter!!.removeItem(mProgress)
            loadMoreVisible = false
        }
    }

    private fun addLoadMore() {
        loadMoreVisible = true
        mAdapter?.insertItem(mProgress)
    }
}