package com.goodwill.tristan.emotivate.ui.screen.main

import com.goodwill.tristan.emotivate.ui.base.BaseNavigationController
import com.goodwill.tristan.emotivate.ui.screen.main.activity.LandingActivity
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.landing.ForgotPasswordFragment
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.landing.LoginFragment
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.landing.RegistrationFragment

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

class LandingNavigationController(landingActivity: LandingActivity) : BaseNavigationController(landingActivity) {

    private val LOGIN_TAG = "login"
    private val REGISTER_TAG = "register"
    private val FORGOT_PASSWORD_TAG = "forgot_password"


    fun goToLogin() {
        addOrChangeFragmentWithBackstack(LoginFragment(), LOGIN_TAG)
    }

    fun goToRegistration() {
        addOrChangeFragmentWithBackstack(RegistrationFragment(), REGISTER_TAG)
    }

    fun goToForgotPassword() {
        addOrChangeFragmentWithBackstack(ForgotPasswordFragment(), FORGOT_PASSWORD_TAG)
    }
}