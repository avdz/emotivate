package com.goodwill.tristan.emotivate.ui.screen.main

import android.os.Bundle
import com.goodwill.tristan.emotivate.ui.base.BaseNavigationController
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.home.*
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.settings.AboutFragment
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.settings.NotificationsFragment
import com.goodwill.tristan.emotivate.ui.screen.main.fragment.settings.SettingsFragment


/**
 * Created by Comp on 27.7.2018..
 */
class MainNavigationController(emotivateMainActivity: EmotivateMainActivity) :
        BaseNavigationController(emotivateMainActivity) {

    private val MAIN = "main"
    private val SETTINGS = "settings"
    private val FAVORITES = "favorites"
    private val AUTHORS = "authors"
    private val ABOUT = "about"
    private val AUTHOR_QUOTES = "author_quotes"
    private val NOTIFICATIONS = "notifications"


    fun goToMainFragment() {
        addOrChangeFragmentWithBackstack(MainFragment(), MAIN)
    }

    fun goToSettingsFragment() {
        addOrChangeFragmentWithBackstack(SettingsFragment(), SETTINGS)
    }

    fun goToAboutFragment() {
        addOrChangeFragmentWithBackstack(AboutFragment(), ABOUT)
    }

    fun goToNotificationsFragment() {
        addOrChangeFragmentWithBackstack(NotificationsFragment(), NOTIFICATIONS)
    }

    fun goToFavoritesFragment() {
        addOrChangeFragmentWithBackstack(FavoritesFragment(), FAVORITES)
    }

    fun goToAuthorsFragment() {
        addOrChangeFragmentWithBackstack(AuthorsFragment(), AUTHORS)
    }

    fun goToAuthorsQuotesFragment(bundle: Bundle) {
        val authorQuotesFragment = AuthorQuotesFragment()
        authorQuotesFragment.arguments = bundle
        addFragmentWithBackstack(authorQuotesFragment, AUTHOR_QUOTES)
    }
}