package com.goodwill.tristan.emotivate.ui.screen.main.activity

import android.content.Intent
import android.view.Menu
import android.view.MenuItem
import android.widget.ImageView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.widget.Toolbar
import com.google.android.material.navigation.NavigationView
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.shared_preferences.UserStorage
import com.goodwill.tristan.emotivate.ui.screen.main.MainNavigationController
import com.goodwill.tristan.emotivate.ui.base.activity.EmotivateBaseActivity
import com.goodwill.tristan.emotivate.utilities.Constants
import com.goodwill.tristan.emotivate.utilities.ViewUtilities
import com.kobakei.ratethisapp.RateThisApp
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_main_drawer.*
import kotlinx.android.synthetic.main.nav_header_main.view.*


/**
 * Created by Comp on 27.7.2018..
 */

class EmotivateMainActivity : EmotivateBaseActivity<MainNavigationController>(),
        NavigationView.OnNavigationItemSelectedListener {

    private var mNavigationMenu : Menu? = null

    override fun initializeScreen() {
        initToolbar(dashboardToolbar)
        initDrawerLayout(dashboardToolbar)
        initNavMenu(dashboardNavigationView)
        initNotifications(notificationsButton)
        getNavigationController()?.goToMainFragment()
        setToolbarTitle(Constants.HOME)
    }

    override fun getFragmentContainerId(): Int {
        return R.id.fragmentContainer
    }

    override fun createNavigationController(): MainNavigationController {
        return MainNavigationController(this)
    }

    override fun getLayoutRId(): Int {
        return R.layout.activity_main_drawer
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        dashboardDrawerLayout.closeDrawers()
        when (item.itemId) {
            R.id.home -> {
                setToolbarTitle(Constants.HOME)
                getNavigationController()?.goToMainFragment()
                return true
            }
            R.id.authors -> {
                setToolbarTitle(Constants.AUTHORS)
                getNavigationController()?.goToAuthorsFragment()
                return true
            }
            R.id.myQuotes -> {
                setToolbarTitle(Constants.MY_QUOTES)
                getNavigationController()?.goToFavoritesFragment()
                return true
            }
            R.id.notifications -> {
                setToolbarTitle(Constants.NOTIFICATIONS)
                getNavigationController()?.goToNotificationsFragment()
                return true
            }
            R.id.aboutUs -> {
                setToolbarTitle(Constants.ABOUT)
                getNavigationController()?.goToAboutFragment()
                return true
            }
            R.id.rateUs -> {
                RateThisApp.showRateDialog(this)
                return true
            }
            else -> {
                return super.onOptionsItemSelected(item)
            }
        }
    }

    fun setToolbarTitle(title: String) {
        dashboardTitle.text = title
    }

    private fun initToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
    }

    private fun initNavMenu(navigationView: NavigationView) {
        navigationView.setNavigationItemSelectedListener(this)
        mNavigationMenu = navigationView.menu
        mNavigationMenu?.findItem(R.id.login)?.let { setupLoginText(it) }
        val headerLayout = navigationView.getHeaderView(0)
        headerLayout.loginContainer.login.setOnClickListener {
            openLogin()
        }
    }

    private fun initDrawerLayout(toolbar: Toolbar) {
        val toggle = ActionBarDrawerToggle(
                this,
                dashboardDrawerLayout,
                toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close)

        dashboardDrawerLayout.addDrawerListener(toggle)
        toggle.syncState()
    }

    private fun initNotifications(notificationsButton: ImageView) {
        notificationsButton.setOnClickListener {
            mNavigationManager.openNotificationsActivity()
        }
    }

    private fun setupLoginText(menuItem: MenuItem) {
        val user = UserStorage.getUser()
        if (user != null) {
            menuItem.title = "Log Out"
            menuItem.setOnMenuItemClickListener {
                logOut()
                true
            }
            setupHeaderUserText(user.nickname, user.username)
        } else {
            menuItem.title = "Login"
            menuItem.setOnMenuItemClickListener {
                openLogin()
                true
            }
        }
    }

    private fun setupHeaderUserText(userNickname : String?,
                                    userEmail : String) {
        val headerLayout = dashboardNavigationView.getHeaderView(0)
        ViewUtilities.showView(headerLayout.userContainer)
        ViewUtilities.hideView(headerLayout.loginContainer)
        headerLayout.userContainer.userEmail.text = userEmail
        if (userNickname != null) {
            ViewUtilities.showView(headerLayout.userContainer.userNickname)
            headerLayout.userContainer.userNickname.text = userNickname
        } else {
            ViewUtilities.hideView(headerLayout.userContainer.userNickname)
        }
    }

    private fun logOut() {
        dashboardDrawerLayout.closeDrawers()
        UserStorage.logOut()
        mNavigationManager.openLandingActivity()
        finish()
    }

    private fun openLogin() {
        mNavigationManager.openLandingActivity()
        finish()
    }

}
