package com.goodwill.tristan.emotivate.ui.screen.main.activity

import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.ui.base.activity.EmotivateBaseActivity
import com.goodwill.tristan.emotivate.ui.screen.main.LandingNavigationController
import com.goodwill.tristan.emotivate.ui.screen.main.MainNavigationController
import kotlinx.android.synthetic.main.activity_landing.view.*

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

class LandingActivity : EmotivateBaseActivity<LandingNavigationController>(){
    override fun getLayoutRId(): Int {
        return R.layout.activity_landing
    }

    override fun initializeScreen() {
        getNavigationController()?.goToLogin()
    }

    override fun getFragmentContainerId(): Int {
        return R.id.fragmentContainer
    }

    override fun createNavigationController(): LandingNavigationController {
        return LandingNavigationController(this)
    }
}