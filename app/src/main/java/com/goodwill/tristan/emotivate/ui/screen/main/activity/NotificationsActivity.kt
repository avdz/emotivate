package com.goodwill.tristan.emotivate.ui.screen.main.activity

import android.os.Bundle
import com.goodwill.tristan.emotivate.R
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.ui.adapters.MenuOptionsAdapter
import com.goodwill.tristan.emotivate.data.realm.MenuRealmCache
import com.goodwill.tristan.emotivate.data.shared_preferences.OnboardingStorage
import com.goodwill.tristan.emotivate.data.shared_preferences.UserStorage
import com.goodwill.tristan.emotivate.models.MenuListModel
import kotlinx.android.synthetic.main.category_picker.*

/**
 * Created by Azra on 9.5.2019.
 */
class NotificationsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.category_picker)
        setStatusBarColor()

        val adapter = MenuOptionsAdapter(this, MenuRealmCache().getMenuListModelsFromCache())

        optionsList.adapter = adapter

        btnConfirm.setOnClickListener {
            for (menuListModel: MenuListModel in adapter.menuListModels) {
                CategoryRealmCache().setCategoryVisible(
                        menuListModel.modelId,
                        menuListModel.modelText,
                        menuListModel.modelVisible)
            }
            val intent = Intent(this, EmotivateMainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            startActivity(intent)
            OnboardingStorage.setOnboardingCompleted()
            finish()
        }
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(R.color.status_bar_color)
        }
    }
}


