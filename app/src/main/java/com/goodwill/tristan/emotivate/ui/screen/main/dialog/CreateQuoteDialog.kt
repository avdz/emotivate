package com.goodwill.tristan.emotivate.ui.screen.main.dialog

import android.app.ActionBar
import android.content.Context
import android.view.View
import android.view.Window
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.CreateQuoteRequest
import com.goodwill.tristan.emotivate.ui.base.dialog.BaseEmotivateDialog
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import kotlinx.android.synthetic.main.dialog_create_quote.*
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 9.11.2019.
 **/

class CreateQuoteDialog(context: Context?,
                        createQuoteCallback: ResponseListener<ResponseBody>,
                        runnable: Runnable) : BaseEmotivateDialog(context) {

    init {
        window.run {
            this?.setBackgroundDrawableResource(android.R.color.transparent)
            this?.requestFeature(Window.FEATURE_NO_TITLE)
            this?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT)

        }
    }

    override val mLayoutRId: Int
        get() = R.layout.dialog_create_quote

    override fun setupDialog() {
        super.setupDialog()
        buttonDone.setOnClickListener(onButtonDone)
    }

    private val onButtonDone = View.OnClickListener {
        runnable.run()
        HttpService.createQuote(CreateQuoteRequest(
                newQuoteText.text.toString().trim(),
                BaseCallback(createQuoteCallback)
        ))
        dismiss()
    }
}