package com.goodwill.tristan.emotivate.ui.screen.main.dialog

import android.content.Context
import android.view.View
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.ui.base.dialog.BaseEmotivateDialog
import com.goodwill.tristan.emotivate.utilities.interfaces.DeleteListener
import kotlinx.android.synthetic.main.dialog_delete.*

/**
 *
 *
 *
 * @author Comp at 10.11.2019.
 **/

class DeleteDialog(context: Context,
                   quote : Quote?,
                   private val deleteListener: DeleteListener?) : BaseEmotivateDialog(context) {
    override val mLayoutRId: Int
        get() = R.layout.dialog_delete

    override fun setupDialog() {
        buttonCancel.setOnClickListener(onCancel)
        buttonDelete.setOnClickListener(onDelete)
    }

    private val onCancel = View.OnClickListener {
        dismiss()
    }

    private val onDelete = View.OnClickListener {
        dismiss()
        deleteListener?.onDelete(quote)
    }
}