package com.goodwill.tristan.emotivate.ui.screen.main.dialog

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.graphics.drawable.AnimationDrawable
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.ui.base.dialog.BaseEmotivateDialog
import kotlinx.android.synthetic.main.dialog_progress.*

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

class ProgressDialog(context: Context) : BaseEmotivateDialog(context) {

    init {
        window.run { this?.setBackgroundDrawableResource(android.R.color.transparent) }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        val spinner = spinnerImageView.background as AnimationDrawable
        spinner.start()
    }

    override val mLayoutRId: Int
        get() = R.layout.dialog_progress

    override fun show() {
        setCancelable(false)
        super.show()
    }

}