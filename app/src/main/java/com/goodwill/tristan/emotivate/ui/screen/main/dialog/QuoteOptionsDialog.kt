package com.goodwill.tristan.emotivate.ui.screen.main.dialog

import android.app.ActionBar
import android.content.Context
import android.view.View
import android.view.Window
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.ui.base.dialog.BaseEmotivateDialog
import com.goodwill.tristan.emotivate.utilities.managers.NavigationManager
import kotlinx.android.synthetic.main.dialog_quote_options.*

/**
 *
 *
 *
 * @author Comp at 10.11.2019.
 **/

class QuoteOptionsDialog(context: Context,
                         quote : Quote) : BaseEmotivateDialog(context) {

    init {
        window.run {
            this?.requestFeature(Window.FEATURE_NO_TITLE)
            this?.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.WRAP_CONTENT);
        }
    }

    override val mLayoutRId: Int
        get() = R.layout.dialog_quote_options

    override fun setupDialog() {
        buttonShare.setOnClickListener(onShare)
        buttonReport.setOnClickListener(onReport)
    }

    private val onShare = View.OnClickListener {
        NavigationManager(getContext()).share(quote.description)
    }

    private val onReport = View.OnClickListener {
        NavigationManager(getContext()).report(quote)
    }
}