package com.goodwill.tristan.emotivate.ui.screen.main.fragment.home

import android.view.View
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.requests.AuthorQuotesRequest
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.utilities.Constants

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-14
 **/
class AuthorQuotesFragment : QuotesFragment() {

    var authorId : String? = null

    override fun initView(view: View) {
        authorId = arguments?.get(Constants.AUTHOR_ID) as String?
        setToolbarTitle("Authors quotes")
        super.initView(view)
    }

    override fun getItems(page: Int) {
        HttpService.getAuthorQuotes(AuthorQuotesRequest(
                authorId!!,
                page,
                perPage,
                mQuery,
                quotesCallback))
    }
}