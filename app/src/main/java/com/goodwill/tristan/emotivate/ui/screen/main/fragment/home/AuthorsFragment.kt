package com.goodwill.tristan.emotivate.ui.screen.main.fragment.home

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.GetAuthorsRequest
import com.goodwill.tristan.emotivate.data.api.requests.QuotesRequest
import com.goodwill.tristan.emotivate.data.api.response.AuthorsResponse
import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.recyclerview.AuthorModel
import com.goodwill.tristan.emotivate.ui.base.fragment.RecyclerViewFragment
import com.goodwill.tristan.emotivate.ui.widgets.SearchView
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.authors_fragment.*
import okhttp3.ResponseBody

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-03
 **/
class AuthorsFragment : RecyclerViewFragment<AuthorModel>() {

    override val mAdView: AdView
        get() = adView

    override val mSearchView: SearchView?
        get() = searchView

    override val mRecyclerView: RecyclerView?
        get() = vfRecyclerView

    override val mLayoutRId: Int
        get() = R.layout.authors_fragment

    override val mSwipeRefresh: SwipeRefreshLayout
        get() = swipeRefresh

    override val mShouldLoadMore: Boolean
        get() = true

    override fun initView(view: View) {
        setToolbarTitle("Authors")
        super.initView(view)
    }

    override fun getItems(page: Int) {
        HttpService.getAuthors(GetAuthorsRequest(
                page,
                perPage,
                mQuery,
                authorsCallback))
    }

    private val authorsCallback = BaseCallback(object : ResponseListener<AuthorsResponse> {

        override fun onSuccess(responseBody: AuthorsResponse) {
            page = responseBody.page
            hasNextPage = responseBody.hasNext
            setupRecyclerView(createAuthorModels(responseBody.authors))
            highlightText()
            stopRefreshing()
        }

        override fun onError(errorResponse : ResponseBody?) {
            stopRefreshing()
        }
    })


    private fun createAuthorModels(authorsList : ArrayList<Author>?) : ArrayList<AuthorModel> {
        val authorModelList = ArrayList<AuthorModel>()
        if (authorsList != null) {
            for (author in authorsList) {
                authorModelList.add(AuthorModel(author))
            }
        }
        return authorModelList
    }

    private fun highlightText() {
        mAdapter?.setHighlightedText(mQuery)
    }
}