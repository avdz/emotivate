package com.goodwill.tristan.emotivate.ui.screen.main.fragment.home

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.QuotesRequest
import com.goodwill.tristan.emotivate.data.shared_preferences.UserStorage
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.ui.base.dialog.InfoDialog
import com.goodwill.tristan.emotivate.ui.screen.main.dialog.CreateQuoteDialog
import com.goodwill.tristan.emotivate.utilities.DialogUtilities
import com.goodwill.tristan.emotivate.utilities.ViewUtilities
import com.goodwill.tristan.emotivate.utilities.interfaces.DeleteListener
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.favorites_fragment.*
import okhttp3.ResponseBody

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-03
 **/
class FavoritesFragment : QuotesFragment(), DeleteListener {

    override fun initView(view: View) {
        super.initView(view)
        buttonAddQuote.setOnClickListener(onAddQuote)
        buttonLogin.setOnClickListener(onLogin)
        setupFragment()
    }

    override fun getItems(page: Int) {
        HttpService.getMyQuotes(QuotesRequest(
                quotesCallback))
    }

    override val mLayoutRId: Int
        get() = R.layout.favorites_fragment

    override val mRecyclerView: RecyclerView?
        get() = vfRecyclerView

    override val mAdView: AdView
        get() = adView

    override val mSwipeRefresh: SwipeRefreshLayout
        get() = swipeRefresh

    override val mViewType: RVModel.ViewType
        get() = RVModel.ViewType.FAVORITE_QUOTE

    override val mShouldLoadMore: Boolean
        get() = false

    private val onAddQuote = View.OnClickListener {
        CreateQuoteDialog(context, createQuoteCallback, Runnable { showProgressDialog() }).show()
    }

    private val onLogin = View.OnClickListener {
        mNavigationManager.openLandingActivity()
    }

    private val createQuoteCallback = object : ResponseListener<ResponseBody> {
        override fun onSuccess(responseBody: ResponseBody) {
            dismissProgressDialog()
            DialogUtilities.showSuccessDialog(
                    context,
                    "Created quote",
                    "Quote created successfully",
                    object : InfoDialog.DialogDismissListener {
                        override fun onDialogDismissed() {
                            onRefresh()
                        }
                    }
            )
        }

        override fun onError(errorResponse: ResponseBody?) {
            dismissProgressDialog()
            DialogUtilities.showErrorDialog(
                    context,
                    "Creating quote failed",
                    errorResponse
            )
        }
    }

    private val deleteQuoteCallback = object : ResponseListener<ResponseBody> {
        override fun onSuccess(responseBody: ResponseBody) {
            dismissProgressDialog()
            DialogUtilities.showSuccessDialog(
                    context,
                    "Delete quote",
                    "Quote deleted successfully",
                    object : InfoDialog.DialogDismissListener {
                        override fun onDialogDismissed() {
                            onRefresh()
                        }
                    }
            )
        }

        override fun onError(errorResponse: ResponseBody?) {
            dismissProgressDialog()
            DialogUtilities.showErrorDialog(
                    context,
                    "Deleting quote failed",
                    errorResponse
            )
        }
    }

    override fun onDelete(quote: Quote?) {
        showProgressDialog()
        HttpService.deleteQuote(quote?.id.toString(),
                BaseCallback(deleteQuoteCallback))
    }

    private fun setupFragment() {
        if (!UserStorage.hasUser()) {
            ViewUtilities.hideView(adView)
            ViewUtilities.hideView(buttonAddQuote)
            ViewUtilities.hideView(swipeRefresh)
        } else {
            ViewUtilities.hideView(textLoginOrCreate)
        }
    }

    override fun createAdapter() {
        super.createAdapter()
        mAdapter?.setDeleteListener(this)
    }


}