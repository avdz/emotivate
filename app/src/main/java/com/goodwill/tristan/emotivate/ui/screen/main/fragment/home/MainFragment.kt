package com.goodwill.tristan.emotivate.ui.screen.main.fragment.home

import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.CategoryQuotesRequest
import com.goodwill.tristan.emotivate.data.api.requests.GetAuthorsRequest
import com.goodwill.tristan.emotivate.data.api.requests.QuotesRequest
import com.goodwill.tristan.emotivate.data.api.response.AuthorsResponse
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.models.recyclerview.AuthorModel
import com.goodwill.tristan.emotivate.models.recyclerview.CategoryModel
import com.goodwill.tristan.emotivate.models.recyclerview.QuoteModel
import com.goodwill.tristan.emotivate.ui.adapters.CategoriesRVAdapter
import com.goodwill.tristan.emotivate.ui.adapters.EmotivateRVAdapter
import com.goodwill.tristan.emotivate.ui.widgets.SearchView
import com.goodwill.tristan.emotivate.utilities.interfaces.OnItemInteractedListener
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.authors_fragment.*
import kotlinx.android.synthetic.main.authors_fragment.adView
import kotlinx.android.synthetic.main.authors_fragment.searchView
import kotlinx.android.synthetic.main.authors_fragment.swipeRefresh
import kotlinx.android.synthetic.main.authors_fragment.vfRecyclerView
import kotlinx.android.synthetic.main.main_fragment.*
import okhttp3.ResponseBody
import java.util.*
import kotlin.collections.ArrayList

/**
 * Created by Azra on 6.8.2018.
 */
class MainFragment : QuotesFragment(), OnItemInteractedListener{

    override val mAdView: AdView
        get() = adView

    override val mSearchView: SearchView?
        get() = searchView

    override val mRecyclerView: RecyclerView?
        get() = vfRecyclerView

    override val mLayoutRId: Int
        get() = R.layout.main_fragment

    override val mSwipeRefresh: SwipeRefreshLayout
        get() = swipeRefresh

    override val mShouldLoadMore: Boolean
        get() = true

    private var mCategoriesAdapter = CategoriesRVAdapter()

    override fun initView(view: View) {
        super.initView(view)
        categoriesContainer?.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true)
        mCategoriesAdapter = CategoriesRVAdapter()
        mCategoriesAdapter.insertItems(createCategoriesModels())
        categoriesContainer.adapter = mCategoriesAdapter
        mCategoriesAdapter.setClickListener(this)
    }

    override fun getItems(page: Int) {
        HttpService.getQuotes(CategoryQuotesRequest(
                page,
                perPage,
                mQuery,
                getSelectedCategory(),
                quotesCallback))
    }

    private fun createCategoriesModels() : ArrayList<CategoryModel> {
        val categories = CategoryRealmCache().getCategories()
        val categoryModels = ArrayList<CategoryModel>()
        if (categories != null) {
            for (category in categories) {
                categoryModels.add(CategoryModel(category, false))
            }
        }
        return categoryModels
    }

    private fun getSelectedCategory() : Int? {
        return mCategoriesAdapter.getSelectedCategory()
    }

    override fun onItemInteracted(index : Int) {
        mCategoriesAdapter.itemSelected(index)
        getItems(1)
    }

}