package com.goodwill.tristan.emotivate.ui.screen.main.fragment.home

import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.response.QuotesResponse
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.models.RVModel
import com.goodwill.tristan.emotivate.models.recyclerview.QuoteModel
import com.goodwill.tristan.emotivate.ui.base.fragment.RecyclerViewFragment
import com.goodwill.tristan.emotivate.ui.widgets.SearchView
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import com.google.android.gms.ads.AdView
import kotlinx.android.synthetic.main.quotes_fragment.*
import kotlinx.android.synthetic.main.quotes_fragment.adView
import kotlinx.android.synthetic.main.quotes_fragment.swipeRefresh
import kotlinx.android.synthetic.main.quotes_fragment.vfRecyclerView
import okhttp3.ResponseBody

/**
 * Created by Comp on 27.9.2018..
 */
abstract class QuotesFragment : RecyclerViewFragment<QuoteModel>(), SearchView.SearchViewListener {

    override val mLayoutRId: Int
        get() = R.layout.quotes_fragment

    override val mAdView: AdView
        get() = adView

    override val mSearchView: SearchView?
        get() = searchView

    override val mRecyclerView: RecyclerView?
        get() = vfRecyclerView

    override val mSwipeRefresh: SwipeRefreshLayout
        get() = swipeRefresh

    override val mShouldLoadMore: Boolean
        get() = true

    open val mViewType = RVModel.ViewType.QUOTE

    protected val quotesCallback = BaseCallback(object : ResponseListener<QuotesResponse> {

        override fun onSuccess(responseBody: QuotesResponse) {
            page = responseBody.page
            hasNextPage = responseBody.hasNext
            setupRecyclerView(createQuoteModels(responseBody.quotes, mViewType))
            highlightText()
            stopRefreshing()
        }

        override fun onError(errorResponse : ResponseBody?) {
            stopRefreshing()
        }
    })

    protected open fun createQuoteModels(quotesList : ArrayList<Quote>?,
                                         viewType : RVModel.ViewType) : ArrayList<QuoteModel> {
        val quoteModelsList = ArrayList<QuoteModel>()
        if (quotesList != null) {
            for (quote : Quote in quotesList) {
                quoteModelsList.add(QuoteModel(quote, viewType))
            }
        }
        return quoteModelsList
    }

    private fun highlightText() {
        mAdapter?.setHighlightedText(mQuery)
    }
}