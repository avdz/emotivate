package com.goodwill.tristan.emotivate.ui.screen.main.fragment.landing

import android.view.View
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.RegisterRequest
import com.goodwill.tristan.emotivate.data.api.requests.ResetPasswordRequest
import com.goodwill.tristan.emotivate.ui.base.dialog.InfoDialog
import com.goodwill.tristan.emotivate.ui.base.fragment.EmotivateBaseFragment
import com.goodwill.tristan.emotivate.ui.screen.main.LandingNavigationController
import com.goodwill.tristan.emotivate.utilities.DialogUtilities
import com.goodwill.tristan.emotivate.utilities.ViewUtilities
import com.goodwill.tristan.emotivate.utilities.exceptions.ContextNotPresentException
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import kotlinx.android.synthetic.main.fragment_forgot_password.*
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 9.11.2019.
 **/

class ForgotPasswordFragment : EmotivateBaseFragment<LandingNavigationController>() {
    override val mLayoutRId: Int
        get() = R.layout.fragment_forgot_password

    override fun initView(view: View) {
        buttonBack.setOnClickListener(onBack)
        buttonConfirm.setOnClickListener(onConfirm)
    }

    private val onConfirm = View.OnClickListener {
        showProgressDialog()
        ViewUtilities.hideView(errorText)
        HttpService.resetPassword(ResetPasswordRequest(
                textEmail.text.trim().toString(),
                textPassword.text.trim().toString(),
                textConfirmPassword.text.trim().toString(),
                BaseCallback(resetPasswordCallback)))
    }

    private val onBack = View.OnClickListener {
        getNavigationController()?.goToLogin()
    }

    private val resetPasswordCallback = object : ResponseListener<ResponseBody> {
        override fun onSuccess(responseBody: ResponseBody) {
            dismissProgressDialog()
            try {
                DialogUtilities.showSuccessDialog(context!!,
                        "Password change request sent",
                        "Please check your email to reset your password.",
                        object : InfoDialog.DialogDismissListener {
                            override fun onDialogDismissed() {
                                getNavigationController()?.goToLogin()
                            }
                        })
            } catch (contextNotPresentException : ContextNotPresentException) {
                contextNotPresentException.printStackTrace()
            }
        }

        override fun onError(errorResponse: ResponseBody?) {
            dismissProgressDialog()
            ViewUtilities.showView(errorText)
            errorText.text = (DialogUtilities.getErrorString(errorResponse))
        }
    }
}