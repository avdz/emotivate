package com.goodwill.tristan.emotivate.ui.screen.main.fragment.landing

import android.animation.Animator
import android.os.CountDownTimer
import android.view.View
import android.widget.Toast
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.LoginRequest
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.data.shared_preferences.UserStorage
import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.User
import com.goodwill.tristan.emotivate.ui.base.fragment.EmotivateBaseFragment
import com.goodwill.tristan.emotivate.ui.screen.main.LandingNavigationController
import com.goodwill.tristan.emotivate.utilities.DialogUtilities
import com.goodwill.tristan.emotivate.utilities.ViewUtilities
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import kotlinx.android.synthetic.main.fragment_login.*
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

class LoginFragment : EmotivateBaseFragment<LandingNavigationController>() {

    private var isNewlyInstantiated = true

    override val mLayoutRId: Int
        get() = R.layout.fragment_login

    override fun initView(view: View) {
        buttonLogin.setOnClickListener(onLogin)
        buttonSignUp.setOnClickListener(onSignUp)
        buttonSkip.setOnClickListener(onSkip)
        buttonForgotPassword.setOnClickListener(onForgotPassword)
        buttonLogin.setOnFocusChangeListener { v, hasFocus ->
            run {
                if (hasFocus) {
                    onLogin.onClick(v)
                }
            }
        }
        if (isNewlyInstantiated) {
            startCountDownTimer(2000, 1000, 1)
        } else {
            startCountDownTimer(0, 0, 0)
        }
    }

    override fun onStop() {
        super.onStop()
        isNewlyInstantiated = false
    }

    private val onLogin = View.OnClickListener {
        ViewUtilities.hideView(errorText)
        showProgressDialog()
        HttpService.login(LoginRequest(
                textEmail.text?.trim().toString(),
                textPassword.text.trim().toString(),
                BaseCallback(loginCallback)))
    }

    private val onForgotPassword = View.OnClickListener {
        getNavigationController()?.goToForgotPassword()
    }

    private val onSignUp = View.OnClickListener {
        getNavigationController()?.goToRegistration()
    }

    private val onSkip = View.OnClickListener {
        mNavigationManager.openMainActivity()
        finishParentActivity()
    }

    private val loginCallback = object : ResponseListener<User> {
        override fun onSuccess(responseBody: User) {
            Toast.makeText(context, "Logged in successfully", Toast.LENGTH_LONG).show()
            UserStorage.saveUser(responseBody)
            CategoryRealmCache().saveCategory(Category(0, "My quotes", true))
            dismissProgressDialog()
            mNavigationManager.openMainActivity()
            finishParentActivity()
        }

        override fun onError(errorResponse: ResponseBody?) {
            dismissProgressDialog()
            ViewUtilities.showView(errorText)
            errorText.text = DialogUtilities.getErrorString(errorResponse)
        }
    }

    private fun startCountDownTimer(countDownDuration : Long,
                                    countDownInterval : Long,
                                    animationDuration : Long) {
        object : CountDownTimer(countDownDuration, countDownInterval) {
            override fun onFinish() {
                ViewUtilities.hideView(loginStartText)
                ViewUtilities.hideView(loadingProgressBar)
                bookIconImageView.setImageResource(R.mipmap.ic_logo_green)
                startAnimation(animationDuration)
            }

            override fun onTick(p0: Long) {}
        }.start()
    }

    private fun startAnimation(durationInSeconds : Long) {
        bookIconImageView.animate().apply {
            x(50f)
            y(100f)
            duration = durationInSeconds * 1000
        }.setListener(object : Animator.AnimatorListener {
            override fun onAnimationRepeat(p0: Animator?) {

            }

            override fun onAnimationEnd(p0: Animator?) {
                ViewUtilities.showView(afterAnimationView)
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationStart(p0: Animator?) {

            }
        })
    }

}