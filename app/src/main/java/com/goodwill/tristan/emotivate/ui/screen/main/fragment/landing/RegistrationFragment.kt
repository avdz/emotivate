package com.goodwill.tristan.emotivate.ui.screen.main.fragment.landing

import android.view.View
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.RegisterRequest
import com.goodwill.tristan.emotivate.ui.base.dialog.InfoDialog
import com.goodwill.tristan.emotivate.ui.base.fragment.EmotivateBaseFragment
import com.goodwill.tristan.emotivate.ui.screen.main.LandingNavigationController
import com.goodwill.tristan.emotivate.utilities.DialogUtilities
import com.goodwill.tristan.emotivate.utilities.ViewUtilities
import com.goodwill.tristan.emotivate.utilities.exceptions.ContextNotPresentException
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import kotlinx.android.synthetic.main.fragment_sign_up.*
import okhttp3.ResponseBody

/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

class RegistrationFragment : EmotivateBaseFragment<LandingNavigationController>() {
    override val mLayoutRId: Int
        get() = R.layout.fragment_sign_up

    override fun initView(view: View) {
        buttonLogin.setOnClickListener(onLogin)
        buttonSignUp.setOnClickListener(onSignUp)
    }

    private val onSignUp = View.OnClickListener {
        showProgressDialog()
        ViewUtilities.hideView(errorText)
        HttpService.register(RegisterRequest(
                textEmail.text.trim().toString(),
                textNickname.text.trim().toString(),
                textPassword.text.trim().toString(),
                textConfirmPassword.text.trim().toString(),
                BaseCallback(registerCallback)))
    }

    private val onLogin = View.OnClickListener {
        getNavigationController()?.goToLogin()
    }

    private val registerCallback = object : ResponseListener<ResponseBody> {
        override fun onSuccess(responseBody: ResponseBody) {
            dismissProgressDialog()
            try {
                DialogUtilities.showSuccessDialog(context!!,
                        "Email registered",
                        "Please check your email to confirm your account.",
                        object : InfoDialog.DialogDismissListener {
                            override fun onDialogDismissed() {
                                getNavigationController()?.goToLogin()
                            }
                        })
            } catch (contextNotPresentException : ContextNotPresentException) {
                contextNotPresentException.printStackTrace()
            }
        }

        override fun onError(errorResponse: ResponseBody?) {
            dismissProgressDialog()
            ViewUtilities.showView(errorText)
            errorText.text = (DialogUtilities.getErrorString(errorResponse))
        }
    }
}