package com.goodwill.tristan.emotivate.ui.screen.main.fragment.settings

import android.view.View
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.ui.screen.main.MainNavigationController
import com.goodwill.tristan.emotivate.ui.base.fragment.EmotivateBaseFragment
import android.content.Intent
import kotlinx.android.synthetic.main.about_fragment.*


/**
 * Created by Azra on 12.5.2019.
 */
class AboutFragment : EmotivateBaseFragment<MainNavigationController>() {

    override fun initView(view: View) {
        mailLink.setOnClickListener {
            startEmailActivity()
        }
        appVersion.text = getAppVersion()
    }

    override val mLayoutRId: Int
        get() = R.layout.about_fragment

    fun startEmailActivity() {
        val intent = Intent(Intent.ACTION_SEND)
        intent.type = "message/rfc822"
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf("app.emotivate@gmail.com"))
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        intent.putExtra(Intent.EXTRA_TEXT, "")
        startActivity(Intent.createChooser(intent, "Send Email using:"))
    }

    fun getAppVersion(): String? {
        val pInfo = context?.packageManager?.getPackageInfo(context?.packageName, 0)
        return "Version " + pInfo?.versionName
    }

}