package com.goodwill.tristan.emotivate.ui.screen.main.fragment.settings

import android.view.View
import android.widget.CompoundButton
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.realm.AlarmRealmCache
import com.goodwill.tristan.emotivate.data.shared_preferences.NotificationsStorage
import com.goodwill.tristan.emotivate.models.AlarmModel
import com.goodwill.tristan.emotivate.services.NotificationAlarmReceiver
import com.goodwill.tristan.emotivate.ui.adapters.AlarmListAdapter
import com.goodwill.tristan.emotivate.ui.base.fragment.EmotivateBaseFragment
import com.goodwill.tristan.emotivate.ui.screen.main.MainNavigationController
import kotlinx.android.synthetic.main.fragment_notifications.*

/**
 *
 *
 *
 * @author Comp at 17.11.2019.
 **/

class NotificationsFragment : EmotivateBaseFragment<MainNavigationController>() {

    override val mLayoutRId: Int
        get() = R.layout.fragment_notifications

    override fun initView(view: View) {
        alarmList.adapter = AlarmListAdapter(context,
                if (AlarmRealmCache().hasAlarms())
                    AlarmRealmCache().getAlarms()?.getAlarmModels()!! else generateNewAlarmList())
        switchSound.setOnCheckedChangeListener(getSwitchCheckedChangedListener())
        switchSound.isChecked = NotificationsStorage.isSoundEnabled()
    }

    private fun generateNewAlarmList(): ArrayList<AlarmModel> {
        val alarmList = ArrayList<AlarmModel>()
        for(index : Int in (0..4)) alarmList.add(AlarmModel())
        return alarmList
    }

    private fun getSwitchCheckedChangedListener() = CompoundButton.OnCheckedChangeListener {
        compoundButton: CompoundButton, isChecked: Boolean ->
        NotificationsStorage.setNotificationsSoundEnabled(isChecked)
    }
}