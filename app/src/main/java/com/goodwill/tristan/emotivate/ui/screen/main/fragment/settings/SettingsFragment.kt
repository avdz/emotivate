package com.goodwill.tristan.emotivate.ui.screen.main.fragment.settings

import android.view.View
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.realm.AlarmRealmCache
import com.goodwill.tristan.emotivate.ui.screen.main.MainNavigationController
import com.goodwill.tristan.emotivate.ui.adapters.AlarmListAdapter
import com.goodwill.tristan.emotivate.ui.base.fragment.EmotivateBaseFragment
import kotlinx.android.synthetic.main.settings_fragment.*
import android.widget.CompoundButton
import com.goodwill.tristan.emotivate.data.shared_preferences.NotificationsStorage
import com.goodwill.tristan.emotivate.models.AlarmModel
import com.goodwill.tristan.emotivate.services.NotificationAlarmReceiver
import com.kobakei.ratethisapp.RateThisApp
import kotlin.collections.ArrayList


/**
 * Created by Azra on 12.5.2019.
 */
class SettingsFragment : EmotivateBaseFragment<MainNavigationController>() {

    override val mLayoutRId: Int
        get() = R.layout.settings_fragment

    override fun initView(view: View) {
        notificationsContainer.setOnClickListener(onNotificationsClick)
        aboutContainer.setOnClickListener(onAboutClick)
        rateContainer.setOnClickListener(onRateUsClick)
    }

    private val onNotificationsClick = View.OnClickListener {
        getNavigationController()?.goToNotificationsFragment()
    }

    private val onAboutClick = View.OnClickListener {
        getNavigationController()?.goToAboutFragment()
    }

    private val onRateUsClick = View.OnClickListener {
        RateThisApp.showRateDialog(context)
    }
}