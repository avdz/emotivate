package com.goodwill.tristan.emotivate.ui.screen.splash

import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.api.requests.GetAuthorsRequest
import com.goodwill.tristan.emotivate.data.realm.AuthorRealmCache
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.data.shared_preferences.OnboardingStorage
import com.goodwill.tristan.emotivate.data.shared_preferences.UserStorage
import com.goodwill.tristan.emotivate.models.Author
import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import okhttp3.ResponseBody

/**
 * Created by Azra on 7.8.2018.
 */
class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash)
        setStatusBarColor()
        HttpService.getCategories(BaseCallback(categoriesCallback))
    }

    private fun setStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.statusBarColor = resources.getColor(R.color.status_bar_color)
        }
    }

    private fun startOnboardingActivity() {
        val intent = Intent(this, OnboardingActivity::class.java)
        startActivity(intent)
        finish()
    }

    private fun startMainActivity() {
        val intent = Intent(this, EmotivateMainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_REORDER_TO_FRONT
        startActivity(intent)
        finish()
    }

    private val categoriesCallback = object : ResponseListener<ArrayList<Category>> {
        override fun onSuccess(responseBody: ArrayList<Category>) {
            if (UserStorage.hasUser()) {
                responseBody.add(Category(0,"My quotes", true))
            }
            CategoryRealmCache().saveCategories(responseBody)
            if (!OnboardingStorage.isOnboardingCompleted()) {
                startOnboardingActivity()
            } else {
                startMainActivity()
            }
        }

        override fun onError(errorResponse : ResponseBody?) {
            if (!OnboardingStorage.isOnboardingCompleted()) {
                startOnboardingActivity()
            } else {
                startMainActivity()
            }
        }
    }
}