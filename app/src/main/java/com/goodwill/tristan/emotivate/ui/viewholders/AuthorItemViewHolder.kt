package com.goodwill.tristan.emotivate.ui.viewholders

import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.bumptech.glide.Glide
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.recyclerview.AuthorModel
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.ui.viewholders.base.BaseViewHolder
import com.goodwill.tristan.emotivate.ui.widgets.EmotivateTextView
import com.goodwill.tristan.emotivate.utilities.Constants
import com.goodwill.tristan.emotivate.utilities.ImageUtilities
import kotlinx.android.synthetic.main.item_author.view.*
import kotlinx.android.synthetic.main.quote_item.view.*

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-09
 **/
class AuthorItemViewHolder(itemView : View) : BaseViewHolder<AuthorModel>(itemView), View.OnClickListener {

    companion object {
        fun getViewHolder(viewGroup: ViewGroup) : AuthorItemViewHolder {
            return AuthorItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.item_author, viewGroup, false
            ))
        }
    }

    fun bind(authorModel : AuthorModel?,
             position : Int) {
        super.bind(authorModel, position)
        setupAuthor()
    }

    private fun setupAuthor() {
        val author = mItem?.author
        itemView.setOnClickListener(this)
        itemView.authorDescription.text = author?.description
        itemView.authorName.text = author?.name
        if (!author?.imageUrl.isNullOrEmpty()) {
            Glide.with(getContext())
                    .load(author?.imageUrl)
                    .placeholder(R.drawable.ic_author_avatar)
                    .into(itemView.authorImage)
        }
        if (mItem?.highlightedText != null) {
            setHighLightedText(itemView.authorName, mItem!!.highlightedText!!)
        }
    }

    override fun onClick(v: View?) {
        val activity = getParentActivity() as EmotivateMainActivity
        val bundle = Bundle()
        bundle.putString(Constants.AUTHOR_ID, mItem?.author?.id.toString())
        activity.getNavigationController()?.goToAuthorsQuotesFragment(bundle)
    }

    private fun setHighLightedText(tv: EmotivateTextView, textToHighlight: String) {
        val tvt = tv.text.toString()
        var ofe = tvt.toLowerCase().indexOf(textToHighlight.toLowerCase())
        val wordToSpan : Spannable = SpannableString(tv.text)
        var ofs = 0
        while (ofs < tvt.length && ofe != -1) {
            ofe = tvt.toLowerCase().indexOf(textToHighlight.toLowerCase(), ofs)
            if (ofe == -1)
                break
            else {
                // set color here
                wordToSpan.setSpan(
                        BackgroundColorSpan(Color.YELLOW),
                        ofe,
                        ofe + textToHighlight.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                wordToSpan.setSpan(
                        ForegroundColorSpan(Color.BLACK),
                        ofe,
                        ofe + textToHighlight.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE)
            }
            ofs = ofe + 1
        }
    }

}