package com.goodwill.tristan.emotivate.ui.viewholders

import android.graphics.Color
import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.recyclerview.CategoryModel
import com.goodwill.tristan.emotivate.ui.viewholders.base.BaseViewHolder
import com.goodwill.tristan.emotivate.utilities.interfaces.OnItemInteractedListener
import kotlinx.android.synthetic.main.item_category.view.*

/**
 *
 *
 *
 * @author Comp at 23.11.2019.
 **/

class CategoryItemViewHolder(itemView : View) : BaseViewHolder<CategoryModel>(itemView), View.OnClickListener {

    companion object {
        fun getViewHolder(viewGroup: ViewGroup) : CategoryItemViewHolder {
            return CategoryItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.item_category, viewGroup, false
            ))
        }
    }

    private var mListener : OnItemInteractedListener? = null

    fun bind(categoryModel: CategoryModel?,
             position : Int,
             listener: OnItemInteractedListener) {
        super.bind(categoryModel, position)
        mListener = listener
        setupCategory()
    }

    private fun setupCategory() {
        val category = mItem?.category
        itemView.setOnClickListener(this)
        if (mItem?.isActive!!) {
            itemView.setBackgroundResource(R.drawable.category_background_active)
        } else{
            itemView.setBackgroundResource(R.drawable.category_background)
        }
        itemView.categoryName.text = category?.name
    }

    override fun onClick(v: View?) {
        mListener?.onItemInteracted(mPosition!!)
    }
}