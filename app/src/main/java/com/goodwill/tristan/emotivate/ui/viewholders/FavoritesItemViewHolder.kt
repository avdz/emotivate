package com.goodwill.tristan.emotivate.ui.viewholders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.api.HttpService
import com.goodwill.tristan.emotivate.data.api.callbacks.base.BaseCallback
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.recyclerview.QuoteModel
import com.goodwill.tristan.emotivate.ui.screen.main.dialog.DeleteDialog
import com.goodwill.tristan.emotivate.ui.viewholders.base.BaseViewHolder
import com.goodwill.tristan.emotivate.utilities.CategoryUtilities
import com.goodwill.tristan.emotivate.utilities.StringUtilities
import com.goodwill.tristan.emotivate.utilities.interfaces.DeleteListener
import com.goodwill.tristan.emotivate.utilities.interfaces.ResponseListener
import com.goodwill.tristan.emotivate.utilities.managers.NavigationManager
import kotlinx.android.synthetic.main.favorites_item.view.*
import okhttp3.ResponseBody

/**
 * Notes:
 *
 *
 *
 * @author dzemal.ibric at 2019-10-31
 **/
class FavoritesItemViewHolder(view : View) : BaseViewHolder<QuoteModel>(view) {

    private var mDeleteListener : DeleteListener? = null

    companion object {
        fun getViewHolder(viewGroup: ViewGroup) : FavoritesItemViewHolder {
            return FavoritesItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.favorites_item, viewGroup, false
            ))
        }
    }

    fun bind(quoteModel : QuoteModel?,
             position : Int,
             deleteListener: DeleteListener?) {
        super.bind(quoteModel, position)
        itemView.quoteText.text = StringUtilities.returnStringInQuotes(quoteModel?.quote?.description)
        itemView.buttonShare.setOnClickListener(onButtonShare)
        itemView.buttonDelete.setOnClickListener(onButtonDelete)
        mDeleteListener = deleteListener
    }

    private val onButtonShare = View.OnClickListener {
        NavigationManager(getContext()).share(mItem?.quote?.description)
    }

    private val onButtonDelete = View.OnClickListener {
        DeleteDialog(getContext(),mItem?.quote, mDeleteListener).show()
    }
}