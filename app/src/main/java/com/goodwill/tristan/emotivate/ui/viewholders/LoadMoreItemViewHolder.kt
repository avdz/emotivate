package com.goodwill.tristan.emotivate.ui.viewholders

import android.annotation.TargetApi
import android.content.res.ColorStateList
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.models.recyclerview.LoadMoreModel
import com.goodwill.tristan.emotivate.ui.viewholders.base.BaseViewHolder

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-09-03
 **/
class LoadMoreItemViewHolder(view : View) : BaseViewHolder<LoadMoreModel>(view) {

    companion object {
        fun getViewHolder(viewGroup: ViewGroup) : LoadMoreItemViewHolder {
            return LoadMoreItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.load_more, viewGroup, false
            ))
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    fun bind(loadMore : LoadMoreModel,
             position : Int) {
        super.bind(loadMore, position)
        view.findViewById<ProgressBar>(R.id.loadMoreProgress).progressTintList =
                ColorStateList.valueOf(getContext().resources.getColor(R.color.status_bar_color))
    }
}