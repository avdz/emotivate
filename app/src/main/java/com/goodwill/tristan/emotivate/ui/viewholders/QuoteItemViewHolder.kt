package com.goodwill.tristan.emotivate.ui.viewholders

import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.data.realm.CategoryRealmCache
import com.goodwill.tristan.emotivate.models.Category
import com.goodwill.tristan.emotivate.models.recyclerview.QuoteModel
import com.goodwill.tristan.emotivate.ui.screen.main.dialog.QuoteOptionsDialog
import com.goodwill.tristan.emotivate.ui.viewholders.base.BaseViewHolder
import com.goodwill.tristan.emotivate.ui.widgets.EmotivateTextView
import kotlinx.android.synthetic.main.quote_item.view.*
import com.goodwill.tristan.emotivate.utilities.CategoryUtilities
import com.goodwill.tristan.emotivate.utilities.ImageUtilities
import com.goodwill.tristan.emotivate.utilities.StringUtilities
import com.goodwill.tristan.emotivate.utilities.ViewUtilities
import com.goodwill.tristan.emotivate.utilities.managers.NavigationManager


/**
 * Created by Comp on 27.9.2018..
 */
class QuoteItemViewHolder(view: View) : BaseViewHolder<QuoteModel>(view) {

    companion object {
        fun getViewHolder(viewGroup: ViewGroup) : QuoteItemViewHolder {
            return QuoteItemViewHolder(LayoutInflater.from(viewGroup.context).inflate(
                    R.layout.quote_item, viewGroup, false
            ))
        }
    }

    fun bind(quoteModel : QuoteModel?,
             position : Int) {
        super.bind(quoteModel, position)
        itemView.optionsButton.setOnClickListener(onOptionsButton)
        setAuthorText()
        setQuoteText()
        setCategoryText()
    }

    private fun setAuthorText() {
        val author = mItem?.quote?.author?.name
        if (!author.isNullOrEmpty()) {
            ViewUtilities.showView(itemView.authorText)
            itemView.authorText.text = mItem?.quote?.author?.name
        } else {
            ViewUtilities.hideView(itemView.authorText)
        }
    }

    private fun setQuoteText() {
        itemView.quoteText.text = StringUtilities.returnStringInQuotes(mItem?.quote?.description)
        if (mItem?.highlightedText != null) {
            setHighLightedText(itemView.quoteText, mItem!!.highlightedText!!)
        }
    }

    private fun setCategoryText() {
        val category = getCategory()
        if (category != null) {
            itemView.categoryName.text = category.name
            val categoryImage = getCategoryImage(category.name)
            if (categoryImage != null) {
                itemView.categoryImage.setImageResource(categoryImage)
            }
        }
    }

    private fun setHighLightedText(tv: EmotivateTextView, textToHighlight: String) {
        val tvt = tv.text.toString()
        var ofe = tvt.toLowerCase().indexOf(textToHighlight.toLowerCase())
        val wordToSpan : Spannable = SpannableString(tv.text)
        var ofs = 0
        while (ofs < tvt.length && ofe != -1) {
            ofe = tvt.toLowerCase().indexOf(textToHighlight.toLowerCase(), ofs)
            if (ofe == -1)
                break
            else {
                // set color here
                wordToSpan.setSpan(
                        BackgroundColorSpan(Color.YELLOW),
                        ofe,
                        ofe + textToHighlight.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                wordToSpan.setSpan(
                        ForegroundColorSpan(Color.BLACK),
                        ofe,
                        ofe + textToHighlight.length,
                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
                tv.setText(wordToSpan, TextView.BufferType.SPANNABLE)
            }
            ofs = ofe + 1
        }
    }

    private fun getCategory() : Category? {
        return CategoryRealmCache().getCategory(mItem?.quote?.categoryId)
    }

    private fun getCategoryImage(categoryName : String?) : Int? {
        return CategoryUtilities.generateIcon(categoryName)
    }


    private val onOptionsButton = View.OnClickListener {
        QuoteOptionsDialog(getContext(), mItem?.quote!!).show()
    }
}