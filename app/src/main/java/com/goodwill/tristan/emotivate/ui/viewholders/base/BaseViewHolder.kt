package com.goodwill.tristan.emotivate.ui.viewholders.base

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.goodwill.tristan.emotivate.utilities.AppUtilities

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-09-03
 **/
abstract class BaseViewHolder<T>(val view : View) : RecyclerView.ViewHolder(view) {

    var mItem : T? = null
    var mPosition : Int? = null

    /**
     * Bind viewholder with view declared in createViewHolder method
     */
    protected fun bind(item : T?,
                       position: Int? = null) {
        mItem = item
        mPosition = position
    }

    protected fun getContext() : Context {
        return view.context
    }

    protected fun getParentActivity() : Activity? {
        return AppUtilities.getActivityFromContext(getContext())
    }

}