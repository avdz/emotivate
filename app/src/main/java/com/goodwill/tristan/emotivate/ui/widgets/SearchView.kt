package com.goodwill.tristan.emotivate.ui.widgets

import android.content.Context
import android.graphics.drawable.Drawable
import android.os.Handler
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.EditText
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.utilities.ResUtilities
import com.goodwill.tristan.emotivate.utilities.StringUtilities

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-03
 **/
class SearchView : EditText {
    private var mSearchViewListener: SearchViewListener? = null
    private val mHandler = Handler()

    private val mRunnable = Runnable {
        if (mSearchViewListener != null) {
            mSearchViewListener!!.onNewSearchQuery(
                    if (!StringUtilities.isEmpty(this@SearchView))
                text.toString()
            else
                "")
        }
    }

    private val searchIconDrawable: Drawable?
        get() = ResUtilities.getDrawable(context, R.drawable.ic_items_search)

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        init()
    }

    private fun init() {
        setActions()
    }

    private fun setActions() {
        setCompoundDrawablesWithIntrinsicBounds(
                searchIconDrawable,
                null,
                null,
                null)
        compoundDrawablePadding = ResUtilities.dp2px(resources, 5f)
    }

    fun setSearchViewListener(searchViewListener: SearchViewListener) {
        mSearchViewListener = searchViewListener
    }

    override fun onTextChanged(text: CharSequence, start: Int, lengthBefore: Int, lengthAfter: Int) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter)
        if (mSearchViewListener == null) {
            return
        }
        if (text.toString().isEmpty()) {
            mSearchViewListener!!.onNewSearchQuery(null)
        } else {
            mHandler.removeCallbacks(mRunnable)
            mHandler.postDelayed(mRunnable, 500)
        }
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        val DRAWABLE_RIGHT = 2
        //getParent().requestDisallowInterceptTouchEvent(true);

        if (event.action == MotionEvent.ACTION_UP) {
            if (compoundDrawables[DRAWABLE_RIGHT] != null) {
                val bounds = compoundDrawables[DRAWABLE_RIGHT].bounds
                if (event.x >= width - bounds.width()) {
                    text = null
                    return true
                }
            }
        }
        return super.onTouchEvent(event)
    }

    interface SearchViewListener {
        fun onNewSearchQuery(query: String?)
    }
}