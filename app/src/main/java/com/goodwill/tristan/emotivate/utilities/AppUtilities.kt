package com.goodwill.tristan.emotivate.utilities

import android.app.Activity
import android.content.Context
import android.content.ContextWrapper

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-04
 **/
class AppUtilities {
    companion object {
        fun getActivityFromContext(context: Context?): Activity? {
            if (context == null) {
                return null
            } else if (context is ContextWrapper) {
                return context as? Activity ?: getActivityFromContext(context.baseContext)
            }

            return null
        }
    }
}