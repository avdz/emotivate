package com.goodwill.tristan.emotivate.utilities

import android.graphics.drawable.Drawable
import com.goodwill.tristan.emotivate.R

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-09
 **/
class CategoryUtilities {


    companion object {
        private const val RELIGION = "Religion"
        private const val FAMILY = "Family"
        private const val GYM = "Gym"
        private const val FRIENDSHIP = "Friendship"
        private const val LIFE = "Life"
        private const val LOVE = "Love"
        private const val WORK = "Work"
        private const val SCHOOL = "School"

        fun generateIcon(name: String?): Int {
            return when (name) {
                RELIGION -> R.drawable.ic_religion
                FAMILY -> R.drawable.ic_family
                GYM -> R.drawable.ic_gym
                FRIENDSHIP -> R.drawable.ic_friendship
                LIFE -> R.drawable.ic_life
                LOVE -> R.drawable.ic_love
                WORK -> R.drawable.ic_work
                SCHOOL -> R.drawable.ic_school
                else -> R.drawable.ic_unknown
            }
        }
    }
}