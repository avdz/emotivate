package com.goodwill.tristan.emotivate.utilities

/**
 * Created by Azra on 11.8.2018.
 */
class Constants {

    companion object {
        //String constants
        const val ONBOARDING_COMPLETED = "onboarding_completed"

        const val GYM_NAME = "GYM"
        const val FRIENDSHIP_NAME = "Friendship"
        const val LIFE_NAME = "Life"
        const val LOVE_NAME = "Love"
        const val FAMILY_NAME = "Family"
        const val WORK_NAME = "Work"
        const val RELIGION_NAME = "Religion"
        const val CATEGORY_ID = "category_id"
        const val ALARM_ID = 8
        const val NOTIFICATION_SOUND = "notification_sound"
        const val NOTIFICATIONS = "Notifications"
        const val HOME = "Home"
        const val SETTINGS = "Settings"
        const val AUTHORS = "Authors"
        const val ABOUT = "About"
        const val MY_QUOTES = "My Quotes"
        const val AUTHOR_ID = "author_id"
        const val USER = "user"
    }
}