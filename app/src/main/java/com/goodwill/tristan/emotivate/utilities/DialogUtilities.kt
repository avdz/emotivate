package com.goodwill.tristan.emotivate.utilities

import android.app.AlertDialog
import android.content.Context
import com.goodwill.tristan.emotivate.data.api.RestApiClient
import com.goodwill.tristan.emotivate.ui.base.dialog.InfoDialog
import com.goodwill.tristan.emotivate.utilities.exceptions.ContextNotPresentException
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import okhttp3.ResponseBody
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.stream.MalformedJsonException
import retrofit2.Converter
import retrofit2.converter.gson.GsonConverterFactory


/**
 *
 *
 *
 * @author Comp at 21.10.2019.
 **/

class DialogUtilities {
    companion object {
        fun showErrorDialog(context: Context?, title: String, error: ResponseBody?) {
            var errorsString = ""
            val entrySet = parseError(error)?.entrySet()

            try {
                if (entrySet != null) {
                    for ((_, value) in entrySet) {
                        errorsString += value.asString + "\n"
                    }
                }
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
            if (context == null) {
                throw ContextNotPresentException()
            }
            showDialog(context, InfoDialog.InfoDialogType.ERROR, title, errorsString)
        }

        fun showSuccessDialog(context: Context?,
                              title: String,
                              text : String,
                              listener : InfoDialog.DialogDismissListener? = null) {
            if (context == null) {
                throw ContextNotPresentException()
            }
            showDialog(context, InfoDialog.InfoDialogType.SUCCESS, title, text, listener)
        }

        private fun showDialog(context: Context,
                               dialogType: InfoDialog.InfoDialogType,
                               title: String,
                               errors: String,
                               listener : InfoDialog.DialogDismissListener? = null) {

            if (!AppUtilities.getActivityFromContext(context)?.isFinishing!!) {
                InfoDialog(context, dialogType, title, errors, listener).show()
            }
        }

        fun parseError(response: ResponseBody?): JsonObject? {
            val error: JsonObject
            try {
                val converter = GsonConverterFactory.create(Gson())
                        .responseBodyConverter(JsonObject::class.java,
                                arrayOfNulls<Annotation>(0),
                                RestApiClient.mRetrofitClient!!) as Converter<ResponseBody, JsonObject>

                error = converter.convert(response!!)!!

            } catch (e: MalformedJsonException) {
                return null
            } catch (e: Exception) {
                return null
            }
            return error
        }

        fun getErrorString(response: ResponseBody?) : String? {
            val jsonObject = parseError(response)
            var errorsString = ""
            val entrySet = jsonObject?.entrySet()

            try {
                if (entrySet != null) {
                    for ((_, value) in entrySet) {
                        errorsString += value.asString + "\n"
                    }
                }
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
            return errorsString
        }
    }
}