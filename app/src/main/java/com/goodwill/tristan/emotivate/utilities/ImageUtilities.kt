package com.goodwill.tristan.emotivate.utilities

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.net.Uri
import android.os.AsyncTask
import android.provider.MediaStore
import android.widget.ImageView
import com.goodwill.tristan.emotivate.models.CategoryImage
import java.io.ByteArrayOutputStream

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-07
 **/
class ImageUtilities {

    companion object {

        fun ModifyImageViewSize(imageView: ImageView, value: Int) {
            imageView.layoutParams.height = imageView.height + value
            imageView.layoutParams.width = imageView.width + value
        }

        fun setImageUrl(imageView: ImageView?, url: String?) {
            DownloadImageTask(imageView).execute(url)
        }

        class DownloadImageTask(internal var bmImage: ImageView?) : AsyncTask<String, Void, Bitmap>() {

            override fun doInBackground(vararg urls: String): Bitmap? {
                val urldisplay = urls[0]
                var mIcon11: Bitmap? = null
                try {
                    val `in` = java.net.URL(urldisplay).openStream()
                    mIcon11 = BitmapFactory.decodeStream(`in`)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                return mIcon11
            }

            override fun onPostExecute(result: Bitmap?) {
                bmImage?.setImageBitmap(result)
            }
        }


        fun RotateBitmap(source: Bitmap,
                         angle: Float,
                         x: Int,
                         y: Int,
                         width: Int,
                         height: Int): Bitmap {
            val matrix = Matrix()
            matrix.postRotate(angle)
            return Bitmap.createBitmap(source,
                    x,
                    y,
                    width,
                    height,
                    matrix, true)
        }

        fun GetUriFromBitmap(inContext: Context, inImage: Bitmap): Uri {
            val bytes = ByteArrayOutputStream()
            inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
            val path = MediaStore.Images.Media.insertImage(
                    inContext.contentResolver,
                    inImage,
                    "Title", null)
            return Uri.parse(path)
        }
    }

    interface ImageDecodeListener {
        fun onImageGenerated(categoryImages : ArrayList<CategoryImage>)
    }
}