package com.goodwill.tristan.emotivate.utilities

import com.goodwill.tristan.emotivate.models.AlarmModel
import io.realm.RealmList

/**
 * Created by Azra on 15.5.2019.
 */
class ListUtilities {
    companion object {

        private val AUTHOR = "author"
        private val QUOTE = "quote"

        fun convertArrayListToRealmList(array: ArrayList<AlarmModel>) : RealmList<*>{
            val realmList = RealmList<AlarmModel>()
            for (index : Int in (0 until array.size)) run {
                realmList.add(array[index])
            }
            return realmList
        }
    }
}