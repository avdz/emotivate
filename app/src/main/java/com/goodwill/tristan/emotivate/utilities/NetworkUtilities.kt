package com.goodwill.tristan.emotivate.utilities

import android.content.Context
import android.net.NetworkInfo
import android.net.ConnectivityManager


/**
 * Created by Azra on 31.5.2019.
 */
class NetworkUtilities {
    companion object {
        fun isInternetConnected(context: Context) : Boolean {
            val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            return connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).state == NetworkInfo.State.CONNECTED ||
                   connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).state == NetworkInfo.State.CONNECTED
        }
    }
}