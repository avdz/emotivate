package com.goodwill.tristan.emotivate.utilities

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.annotation.DrawableRes
import kotlin.math.roundToInt

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-03
 **/
class ResUtilities {
    companion object {
        fun getDrawable(context: Context, @DrawableRes drawableResource: Int): Drawable? {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                context.getDrawable(drawableResource)
            } else {
                context.resources.getDrawable(drawableResource)
            }
        }

        fun dp2px(resources: Resources, dp: Float): Int {
            val scale = resources.displayMetrics.density
            return (dp * scale + 0.5f).roundToInt()
        }
    }
}