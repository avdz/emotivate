package com.goodwill.tristan.emotivate.utilities

import android.widget.TextView

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 26/03/2019
 * <dzemal.ibric@klika.ba>
 */
class StringUtilities {
    companion object {
        fun returnStringInQuotes(string: String?) : String {
            return '"' + string.toString() + '"'
        }

        fun convertTimeToString(hour:Int, min:Int) : String {
            val hourString = if (hour in (0..9)) "0$hour" else hour.toString()
            val minString = if (min in (0..9)) "0$min" else min.toString()
            return "$hourString:$minString"
        }

        fun extractHourFromString(time : String) : Int {
            return time.split(":").toTypedArray()[0].toInt()
        }

        fun extractMinuteFromString(time : String) : Int {
            return time.split(":").toTypedArray()[1].toInt()
        }

        fun isEmpty(textView: TextView?): Boolean {
            return textView == null || textView.text.toString().isEmpty()
        }

        fun containsIgnoreCase(string: String?, searchString: String?): Boolean {
            if (string == null || searchString == null) return false

            val length = searchString.length
            if (length == 0)
                return true

            for (i in string.length - length downTo 0) {
                if (string.regionMatches(i, searchString, 0, length, ignoreCase = true))
                    return true
            }
            return false
        }
    }
}