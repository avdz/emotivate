package com.goodwill.tristan.emotivate.utilities

import android.content.Context
import android.graphics.Typeface
import java.util.*

/**
 * Notes:
 *  <p>
 *    //Write notes here
 *  </p>
 * ------------------------------------------------------------------
 * @author: Džemal Ibrić
 * 26/03/2019
 * <dzemal.ibric@klika.ba>
 */
class TypeFaceProvider {
    val TYPEFACE_FOLDER = "fonts"
    val TYPEFACE_EXTENSION = ".ttf"

    companion object {
        private val sTypeFaces = Hashtable<String, Typeface>(10)

        fun getTypeFace(context: Context, fileName: String): Typeface {
            var tempTypeface: Typeface? = sTypeFaces[fileName]

            if (tempTypeface == null) {
                tempTypeface = Typeface.createFromAsset(context.assets, fileName)
                sTypeFaces[fileName] = tempTypeface!!
            }

            return tempTypeface
        }
    }
}