package com.goodwill.tristan.emotivate.utilities

import android.view.View

/**
 *
 *
 *
 * @author Comp at 21.10.2019.
 **/

class ViewUtilities {
    companion object {
        fun showView(view : View?) {
            view?.visibility = View.VISIBLE
        }

        fun hideView(view : View?) {
            view?.visibility = View.GONE
        }
    }
}