package com.goodwill.tristan.emotivate.utilities.exceptions

import java.lang.Exception

/**
 *
 *
 *
 * @author Comp at 21.10.2019.
 **/

class ContextNotPresentException : Exception(){

    override val cause: Throwable?
        get() = Throwable("Context is not present!")

    override val message: String?
        get() = "Context is null!"
}