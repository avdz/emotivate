package com.goodwill.tristan.emotivate.utilities.interfaces

import com.goodwill.tristan.emotivate.models.Quote

/**
 *
 *
 *
 * @author Comp at 10.11.2019.
 **/
interface DeleteListener {
    fun onDelete(quote: Quote?)
}