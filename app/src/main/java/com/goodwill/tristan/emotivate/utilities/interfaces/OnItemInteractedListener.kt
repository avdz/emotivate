package com.goodwill.tristan.emotivate.utilities.interfaces

import com.goodwill.tristan.emotivate.models.recyclerview.CategoryModel

/**
 *
 *
 *
 * @author Comp at 23.11.2019.
 **/

interface OnItemInteractedListener {
    fun onItemInteracted(index : Int)
}