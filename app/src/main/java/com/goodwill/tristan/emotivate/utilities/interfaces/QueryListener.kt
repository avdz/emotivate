package com.goodwill.tristan.emotivate.utilities.interfaces

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-10-04
 **/
interface QueryListener {
    fun getNewQuery(query : String?)
}