package com.goodwill.tristan.emotivate.utilities.interfaces

import okhttp3.ResponseBody

/**
 * Notes :
 *
 *
 *
 * @author dzemal.ibric@klika.ba on 2019-08-23
 **/
interface ResponseListener<T> {
    fun onSuccess(responseBody : T)
    fun onError(errorResponse : ResponseBody?)
}