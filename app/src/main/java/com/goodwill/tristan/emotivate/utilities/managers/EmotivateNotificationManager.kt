package com.goodwill.tristan.emotivate.utilities.managers

import android.annotation.TargetApi
import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.services.NotificationAlarmReceiver
import java.util.*

/**
 * Created by Comp on 14.5.2019..
 */
class EmotivateNotificationManager(val mContext: Context?) {

    val alarmManager = mContext?.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    val alarmIntent = Intent(mContext, NotificationAlarmReceiver::class.java)

    fun setNotificationAlarm(hour: Int,
                             minute: Int,
                             alarmId: Int) {

        val alarmStartTime = Calendar.getInstance()
        val now = Calendar.getInstance()
        alarmStartTime.set(Calendar.HOUR_OF_DAY, hour)
        alarmStartTime.set(Calendar.MINUTE, minute)
        if (now.after(alarmStartTime)) {
            alarmStartTime.add(Calendar.DATE, 1)
        }

        alarmManager.setInexactRepeating(
                AlarmManager.RTC_WAKEUP,
                alarmStartTime.timeInMillis,
                AlarmManager.INTERVAL_DAY,
                createPendingIntent(alarmId))
    }

    fun cancelAlarm(alarmId: Int) {
        alarmManager.cancel(getPendingIntent(alarmId))
    }

    fun createPendingIntent(alarmId: Int): PendingIntent {
        return PendingIntent.getBroadcast(
                mContext, alarmId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun getPendingIntent(alarmId: Int): PendingIntent {
        return PendingIntent.getBroadcast(
                mContext, alarmId, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)
    }

    fun setupNotificationChannels() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val notificationManager = mContext?.getSystemService(NotificationManager::class.java)
            if (notificationManager != null) {
                val channelList = ArrayList<NotificationChannel>()
                channelList.add(createEmotivateNotificationChannel())
                notificationManager.createNotificationChannels(channelList)
            }
        }
    }

    @TargetApi(26)
    private fun createNotificationChannel(id: String?,
                                          description: String?,
                                          name: String?,
                                          importance: Int): NotificationChannel {
        val channel = NotificationChannel(
                id,
                name,
                importance)
        channel.description = description

        return channel
    }

    @TargetApi(24)
    private fun createEmotivateNotificationChannel(): NotificationChannel {
        return createNotificationChannel(
                mContext?.getString(R.string.emotivate_channel_id),
                mContext?.getString(R.string.emotivate_channel_description),
                mContext?.getString(R.string.emotivate_channel_name),
                NotificationManager.IMPORTANCE_DEFAULT)
    }

}
