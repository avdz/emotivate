package com.goodwill.tristan.emotivate.utilities.managers

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.goodwill.tristan.emotivate.R
import com.goodwill.tristan.emotivate.ui.screen.main.activity.EmotivateMainActivity
import com.goodwill.tristan.emotivate.ui.screen.main.activity.LandingActivity
import android.widget.Toast
import androidx.core.content.ContextCompat.startActivity
import com.goodwill.tristan.emotivate.models.Quote
import com.goodwill.tristan.emotivate.ui.screen.main.activity.NotificationsActivity


/**
 *
 *
 *
 * @author Comp at 19.10.2019.
 **/

class NavigationManager(private val mContext: Context) {

    private fun startActivity(javaClass : Class<out Activity>, flags : Int = 0) {
        val intent = Intent(mContext, javaClass)
        if (flags != 0) {
            intent.flags = flags
        }
        mContext.startActivity(intent)
    }

    fun openMainActivity() {
        startActivity(EmotivateMainActivity::class.java)
    }

    fun openLandingActivity() {
        startActivity(LandingActivity::class.java)
    }

    fun openNotificationsActivity() {
        startActivity(NotificationsActivity::class.java)
    }

    fun share(quote : String?) {
        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Emotivate")
        sharingIntent.putExtra(Intent.EXTRA_TEXT, quote)
        mContext.startActivity(Intent.createChooser(
                sharingIntent, mContext.resources.getString(R.string.share_via)))
    }

    fun report(quote : Quote) {
        val i = Intent(Intent.ACTION_SEND)
        i.type = "message/rfc822"
        i.putExtra(Intent.EXTRA_EMAIL, arrayOf("app.emotivate@gmail.com"))
        i.putExtra(Intent.EXTRA_SUBJECT, "Reporting quote")
        i.putExtra(Intent.EXTRA_TEXT, "Dear Emotivate, \nI would like to report this quote because:\n\n\nSincerely.\n\n\nQuote id : " + quote.id)
        try {
            mContext.startActivity(Intent.createChooser(i, "Send mail..."))
        } catch (ex: android.content.ActivityNotFoundException) {
            Toast.makeText(mContext, "There are no email clients installed.", Toast.LENGTH_SHORT).show()
        }

    }

}